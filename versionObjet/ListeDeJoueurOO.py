#!/usr/bin/python
#-*- coding: utf-8 -*-

import random
from JoueurOO import *

class ListeDeJoueurOO:
    def __init__(self, nomsJoueur):
        self.ListeJoueurs = []
        for nom in nomsJoueur:
            self.ajouteJoueur(nom)
        self.JoueurCourant = 1

    def ajouteJoueur(self, nom):
        """
        ajoute un nouveau joueur à la fin de la liste
            paramètres: joueurs une liste de joueurs
                        joueur le joueur à ajouter
            cette fonction ne retourne rien mais modifie la liste des joueurs
        """
        Joueur = JoueurOO(nom)
        self.ListeJoueurs.append(Joueur)

    def initAleatoireJoueurCourant(self):
        """
        tire au sort le joueur courant
            paramètre: joueurs un liste de joueurs
            cette fonction ne retourne rien mais modifie la liste des joueurs
        """
        num_random = random.randint(1,self.getNbJoueurs())
        self.JoueurCourant = num_random

    def distribuerTresors(self, nbTresors = 24, nbTresorMax = 0):
        """
        distribue de manière aléatoire des trésors entre les joueurs.
            paramètres: joueurs la liste des joueurs
                        nbTresors le nombre total de trésors à distribuer (on rappelle 
                                que les trésors sont des entiers de 1 à nbTresors)
                        nbTresorsMax un entier fixant le nombre maximum de trésor 
                                     qu'un joueur aura après la distribution
                                     si ce paramètre vaut 0 on distribue le maximum
                                     de trésor possible  
            cette fonction ne retourne rien mais modifie la liste des joueurs
        """
        nbJoueurs = self.getNbJoueurs()
        numJoueur = 0
        nb_tresor_max=0
        if nbTresorMax==0 or nbTresorMax*nbJoueurs>nbTresors:
            nb_tresor_max=nbTresors//nbJoueurs
        else:
            nb_tresor_max=nbTresorMax
        tresor_a_distribuer=random.sample(range(1,nb_tresor_max*nbJoueurs+1),nb_tresor_max*nbJoueurs)
        
        
        for i in tresor_a_distribuer:
            self.getJoueur(numJoueur).AjouteTresor(i)
            numJoueur = (numJoueur+1)%nbJoueurs

    def changerJoueurCourant(self):
        """
        passe au joueur suivant (change le joueur courant donc)
            paramètres: joueurs la liste des joueurs
            cette fonction ne retourne rien mais modifie la liste des joueurs
        """
        num_joueur = self.JoueurCourant
        num_joueur = ((num_joueur)%self.getNbJoueurs())+1
        self.JoueurCourant = num_joueur

    def getNbJoueurs(self):
        """
        retourne le nombre de joueurs participant à la partie
            paramètre: joueurs la liste des joueurs
            résultat: le nombre de joueurs de la partie
        """
        return len(self.ListeJoueurs)

    def getJoueurCourant(self):
        """
        retourne le joueur courant
            paramètre: joueurs la liste des joueurs
            résultat: le joueur courant
        """
        return self.ListeJoueurs[self.JoueurCourant-1]
    
    def getJoueur(self, num_joueur):
        # J'ai mis un -1 pour tester et c'est passé mais je ne sais pas pourquoi /!\
        return self.ListeJoueurs[num_joueur-1]

    def joueurCourantTrouveTresor(self):
        """
        Met à jour le joueur courant lorsqu'il a trouvé un trésor
            c-à-d enlève le trésor de sa liste de trésors à trouver
            paramètre: joueurs la liste des joueurs
            cette fonction ne retourne rien mais modifie la liste des joueurs
        """
        self.getJoueurCourant().tresorTrouve()
        
    def nbTresorsRestantsJoueur(self, numJoueur):
        """
        retourne le nombre de trésors restant pour le joueur dont le numéro 
            est donné en paramètre
            paramètres: joueurs la liste des joueurs
                        numJoueur le numéro du joueur
            résultat: le nombre de trésors que joueur numJoueur doit encore trouver
        """
        Joueur = self.getJoueur(numJoueur)
        return Joueur.getNbTresor()
        

    def numJoueurCourant(self):
        """
        retourne le numéro du joueur courant
            paramètre: joueurs la liste des joueurs
            résultat: le numéro du joueur courant
        """
        return self.JoueurCourant

    def nomJoueurCourant(self):
        """
        retourne le nom du joueur courant
            paramètre: joueurs la liste des joueurs
            résultat: le nom du joueur courant
        """
        return self.getJoueurCourant().getNom()

    def nomJoueur(self, numJoueur):
        """
        retourne le nom du joueur dont le numero est donné en paramètre
            paramètres: joueurs la liste des joueurs
                        numJoueur le numéro du joueur    
            résultat: le nom du joueur numJoueur
        """
        return self.getJoueur(numJoueur).getNom()

    def prochaineTresorJoueur(self, numJoueur):
        """
        retourne le trésor courant du joueur dont le numero est donné en paramètre
            paramètres: joueurs la liste des joueurs
                        numJoueur le numéro du joueur    
            résultat: le prochain trésor du joueur numJoueur (un entier)
        """
        return self.getJoueur(numJoueur).prochainTresor()

    def tresorCourant(self):
        """
        retourne le trésor courant du joueur courant
            paramètre: joueurs la liste des joueurs 
            résultat: le prochain trésor du joueur courant (un entier)
        """
        return self.getJoueurCourant().prochainTresor()
    
    def joueurCourantAFini(self):
        """
        indique blablabla
        """
        if (self.getJoueurCourant().getNbTresor()==0):
            return True
        return False
    

if __name__=="__main__":
    
    LS = ListeDeJoueurOO(["bob","peter"])
    for elem in LS.ListeJoueurs:
        print(elem.getNom())
    LS.distribuerTresors()
    #print(LS.getJoueurCourant().prochainTresor())
    #print(LS.getJoueurCourant().getNbTresor())
    LS.initAleatoireJoueurCourant()
    #print(LS.nomJoueurCourant())
    

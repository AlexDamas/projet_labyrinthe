#!/usr/bin/python
#-*- coding: utf-8 -*-

import random
from MatriceOO import *
from CarteOO import *

class PlateauOO:
    def __init__(self,nbJoueurs, nbTresors):
        defaultcard = CarteOO(True,True,True,True,0,[])
        matrice= MatriceOO(7,7,defaultcard)
        tresor_debut=1
        
        matrice.setVal(0,0,CarteOO(True,False,False,True,0,[]))
        matrice.setVal(0,6,CarteOO(True,True,False,False,0,[]))
        matrice.setVal(6,6,CarteOO(False,True,True,False,0,[]))
        matrice.setVal(6,0,CarteOO(False,False,True,True,0,[]))
        
        matrice.setVal(0,2,CarteOO(True,False,False,False,0,[]))
        matrice.setVal(0,4,CarteOO(True,False,False,False,0,[]))
        matrice.setVal(6,2,CarteOO(False,False,True,False,0,[]))
        matrice.setVal(6,4,CarteOO(False,False,True,False,0,[]))
        
        matrice.setVal(2,0,CarteOO(False,False,False,True,0,[]))
        matrice.setVal(2,2,CarteOO(False,False,False,True,0,[]))
        matrice.setVal(2,4,CarteOO(True,False,False,False,0,[]))
        matrice.setVal(2,6,CarteOO(False,False,True,False,0,[]))
        
        matrice.setVal(4,0,CarteOO(False,False,False,True,0,[]))
        matrice.setVal(4,2,CarteOO(False,False,True,False,0,[]))
        matrice.setVal(4,4,CarteOO(False,True,False,False,0,[]))
        matrice.setVal(4,6,CarteOO(False,True,False,False,0,[]))
        
        # Positionnement des trésors sur les cartes fixes, le tuple (None,None) est un Teneur de Place moche je corrigerai ça plus tard
        
        liste_carte_fixes_avec_tresors = [(None,None),(0,2),(0,4),(2,0),(2,2),(2,4),(2,6),(4,0),(4,2),(4,4),(4,6),(6,2),(6,4)]
        
        for i in range(1,nbTresors+1):
            if i <= 12:
                coords = liste_carte_fixes_avec_tresors[i]
                matrice.getVal(coords[0],coords[1]).mettreTresor(i)
        
        # Je ne sais plus à quoi cela sert....
        
        if nbTresors <= 12:
            tresor_debut = 0
            nbTresors = 0
        else:
            tresor_debut = 13
        
        liste_amovible = self.creerCartesAmovibles(tresor_debut, nbTresors+1)
        indice_amovible = 0
        
        # Rempli les carte par défaut avec des cartes amovibles
        
        for i in range(7):
            j = 0
            for j in range(7):
                #print(coderMurs(plate['Matrice'][i][j]))
                
                # Ici == 15 fait référence au code de la carte par défaut
                if matrice.getVal(i,j).coderMurs() == 15:
                    matrice.setVal(i,j,liste_amovible[indice_amovible])
                    indice_amovible+=1
        
        # Cette partie est factorisable! #YakaFokon # C'est fait Youpi!!
        
        if nbJoueurs >= 1:
            matrice.getVal(0,0).setListePion([1])
        if nbJoueurs >= 2:
            matrice.getVal(6,6).setListePion([2])
        if nbJoueurs >= 3:
            matrice.getVal(6,0).setListePion([3])
        if nbJoueurs == 4:
            matrice.getVal(0,6).setListePion([4])
        
        
        
        ## J'ai transformé plateau en liste plutôt que tuple pour pouvoir changer la valeur de la carte à jouer LOL! o.O #Rémy grand maitre batracien
        #return [plateau,liste_amovible[-1]]#retourne un tuple avec un dico il me semble. Est-ce normal? Ui :-}
        self.Matrice = matrice

        self.carteAjouer = liste_amovible[-1]
    
    def getMatrice(self):
        return self.Matrice

    def print_labyrinthe(self):
        """
        Double boucle de test pour afficher le rendu du labyrinthe en ascii 
        Peut être prévoir d'individualiser ce code dans une fonction car ça pourrait resservir au débuggage ?
        """
        # Ne marchera pas en POO ..voir pour refaire...
        i = 0
        for i in range(7):
            j = 0
            line = ''
            for j in range(7):
            
                line += listeCartes[self.coderMurs(plateau['Matrice'][i][j])]

    def creerCartesAmovibles(self,tresorDebut,nbTresors):
        """
        fonction utilitaire qui permet de créer les cartes amovibles du jeu en y positionnant
            aléatoirement nbTresor trésors
            la fonction retourne la liste, mélangée aléatoirement, des cartes ainsi créées
            paramètres: tresorDebut: le numéro du premier trésor à créer
                        nbTresors: le nombre total de trésor à créer
            résultat: la liste mélangée aléatoirement des cartes amovibles créees
        """
        pool_carte = [1]*6
        carte_L = [3]*16
        carte_I = [5]*12
        pool_carte.extend(carte_L)
        pool_carte.extend(carte_I)
    
        pool_carte_melangees = random.sample(pool_carte, 34)
        
        self.listesCartes=pool_carte_melangees
        
        #
        #Boucle permettant de "Tourner" les "cartes" de façon aléatoire
        #
        
        for indice in range(len(self.listesCartes)):
            
            if self.listesCartes[indice] == 1:
                c = random.sample({1,2,4,8},1)
            if self.listesCartes[indice] == 3:
                c = random.sample({3,6,9,12},1)
            if self.listesCartes[indice] == 5:
                c = random.sample({5,10},1)
            
            self.listesCartes[indice] = c[0]
        
        # Voir pour réunir le haut et le bas ici !
        
        liste_carte_melangees = []
        for indice in self.listesCartes:
            liste_carte_melangees.append(CarteOO(True,True,True,True,0,[]))
            liste_carte_melangees[-1].decoderMurs(indice)
            #print("\n###",listeCartes[coderMurs(liste_carte_melangees[-1])])
        
        #
        # Intégration des trésors dans la liste de carte
        #
        #print(tresorDebut)
        #print(nbTresors)
        liste_tresor = range(tresorDebut, nbTresors)
        liste_tresor = random.sample(liste_tresor, len(liste_tresor))
        #print(liste_tresor)
        
        positions_tresors = random.sample(range(34),len(liste_tresor))
        #print(positions_tresors)
        indice = 0
        for indice in range(len(positions_tresors)):
            liste_carte_melangees[positions_tresors[indice]].mettreTresor(liste_tresor[indice])
        liste_carte_melangees=liste_carte_melangees
        #print(liste_carte_melangees)
        return liste_carte_melangees
    
    #Ligne de test
    #tresor_debut=0
    #tresor_debut.creerCartesAmovibles(12)

    #P = Plateau(4,12)
    #print_labyrinthe(P[0])
    #P0 = P[0]
    #decalageLigneAGauche(P0,2,P[1])
    #decalageLigneAGauche(P0,2,P[1])
    #decalageLigneAGauche(P0,2,P[1])
    #decalageLigneAGauche(P0,2,P[1])
    #print_labyrinthe(P[0])

    def prendreTresorPlateau(self, lig, col, numTresor):
        """
        prend le tresor numTresor qui se trouve sur la carte en lin,col du plateau
            retourne True si l'opération s'est bien passée (le trésor était vraiment sur
            la carte
            paramètres: plateau: le plateau considéré
                        lig: la ligne où se trouve la carte
                        col: la colonne où se trouve la carte
                        numTresor: le numéro du trésor à prendre sur la carte
            resultat: un booléen indiquant si le trésor était bien sur la carte considérée
        """
        carte = self.Matrice.getVal(lig,col)
        tresor = carte.getTresor()
        if tresor == 0:
            return False
        else:
            if tresor != numTresor:
                return False
            else:
                carte.prendreTresor()
                try:
                    carte.getTresor() == 0
                except:
                    print("Erreur script pour enlever le tresor d'une carte du plateau")
                return True

    def getCoordonneesTresor(self, numTresor):
        """
        retourne les coordonnées sous la forme (lig,col) du trésor passé en paramètre
            paramètres: plateau: le plateau considéré
                        numTresor: le numéro du trésor à trouver
            resultat: un couple d'entier donnant les coordonnées du trésor ou None si
                      le trésor n'est pas sur le plateau
        """
        #print(plateau[0])
        
        for i in range(7):
            j = 0
            for j in range(7):
                carte = self.Matrice.getVal(i, j)
                if carte.getTresor() == numTresor:
                    return (i,j)
        return None

    def getCoordonneesJoueur(self, numJoueur):
        """
        retourne les coordonnées sous la forme (lig,col) du joueur passé en paramètre
            paramètres: plateau: le plateau considéré
                        numJoueur: le numéro du joueur à trouver
            resultat: un couple d'entier donnant les coordonnées du joueur ou None si
                      le joueur n'est pas sur le plateau
        """
        for i in range(7):
            j = 0
            for j in range(7):
                
                carte = self.Matrice.getVal(i,j)
                ls_pion = carte.getListePions()
                #print(ls_pion)
                if numJoueur in ls_pion:

                    return (i,j)
        return None


    def prendrePionPlateau(self, lin, col, numJoueur):
        """
        prend le pion du joueur sur la carte qui se trouve en (lig,col) du plateau
            paramètres: plateau:le plateau considéré
                        lin: numéro de la ligne où se trouve le pion
                        col: numéro de la colonne où se trouve le pion
                        numJoueur: le numéro du joueur qui correspond au pion
            Cette fonction ne retourne rien mais elle modifie le plateau
        """
        carte = self.Matrice.getVal(lin,col)
        if carte.possedePion(numJoueur):
            carte.prendrePion( numJoueur)

    def poserPionPlateau(self, lin, col, numJoueur):
        """
        met le pion du joueur sur la carte qui se trouve en (lig,col) du plateau
            paramètres: plateau:le plateau considéré
                        lin: numéro de la ligne où se trouve le pion
                        col: numéro de la colonne où se trouve le pion
                        numJoueur: le numéro du joueur qui correspond au pion
            Cette fonction ne retourne rien mais elle modifie le plateau
        """
        carte = self.Matrice.getVal(lin,col)
        #print(carte)
        carte.poserPion( numJoueur)

    def accessible(self, ligD, colD, ligA, colA):
        """
        indique si il y a un chemin entre la case ligD,colD et la case ligA,colA du labyrinthe
            paramètres: plateau: le plateau considéré
                        ligD: la ligne de la case de départ
                        colD: la colonne de la case de départ
                        ligA: la ligne de la case d'arrivée
                        colA: la colonne de la case d'arrivée
            résultat: un boolean indiquant s'il existe un chemin entre la case de départ
                      et la case d'arrivée
        """
        matrice = self.Matrice
        calque = self.Matrice(7,7,0)
        calque.setVal(ligD,colD,1)
        
        l_max = 7
        c_max = 7
        valeur = None
        
        marquer = True
        while marquer:
            marquer = False
            for ligne in range(l_max):
                colonne = 0
                for colonne in range(c_max):
                    
                    Nord = ligne-1
                    Sud = ligne+1
                    Est = colonne+1
                    Ouest = colonne-1
                    
                    if calque.getVal(ligne,colonne)!=0:
                        
                        if Nord >= 0 and matrice.getVal(ligne,colonne).passageNord(matrice.getVal(Nord,colonne)) and calque.getVal(Nord,colonne)==0:
                            
                            marquer = True
                            
                            valeur = calque.getVal(ligne,colonne)
                            calque.setVal(Nord,colonne,valeur+1)
                            
                            #print("Nord")
                        
                        if Ouest >= 0 and matrice.getVal(ligne, colonne).passageOuest(matrice.getVal(ligne, Ouest)) and calque.getVal(ligne,Ouest)==0:
                            
                            marquer = True
                            
                            valeur = calque.getVal( ligne, colonne)
                            calque.setVal( ligne, Ouest, valeur+1)
                            
                            #print("Ouest")
                        
                        if Sud < l_max and matrice.getVal(ligne, colonne).passageSud(matrice.getVal(Sud, colonne)) and calque.getVal(Sud,colonne)==0:
                            
                            marquer = True
                            
                            valeur = calque.getVal( ligne, colonne)
                            calque.setVal( Sud, colonne, valeur+1)
                            
                            #print("Sud")
                        

                        if Est < c_max and matrice.getVal(ligne, colonne).passageEst(matrice.getVal(ligne, Est)) and calque.getVal(ligne,Est)==0:
                            
                            marquer = True
                            
                            valeur = calque.getVal( ligne, colonne)
                            calque.setVal( ligne, Est, valeur+1)
                            
                            #print("Est")
                        
        if calque.getVal(ligA,colA)!=0 and calque.getVal(ligD,colD)!=0:
            return True
        return False

    def accessibleDist(self, ligD, colD, ligA, colA):
        """
        indique si il y a un chemin entre la case ligD,colD et la case ligA,colA du plateau
            mais la valeur de retour est None s'il n'y a pas de chemin, 
            sinon c'est un chemin possible entre ces deux cases sous la forme d'une liste
            de coordonées (couple de (lig,col))
            paramètres: plateau: le plateau considéré
                        ligD: la ligne de la case de départ
                        colD: la colonne de la case de départ
                        ligA: la ligne de la case d'arrivée
                        colA: la colonne de la case d'arrivée
            résultat: une liste de coordonées indiquant un chemin possible entre la case
                      de départ et la case d'arrivée
        """
        ####################################################################
        ### Cette fonction est beaucoup trop longue pour être maintenue  ###
        ### Efficacement, donc si vous avec une après midi a y consacrer ###
        ### Vous pouvez tenter de la factoriser en écrivant par exemple  ###
        ### Quelques fonctions auxiliaires, .o?                          ###
        ####################################################################
        matrice = self.getMatrice()
        calque = MatriceOO(7,7,0)
        calque.setVal(ligD,colD,1)
        
        l_max = 7
        c_max = 7
        valeur = None
        
        marquer = True
        while marquer:
            marquer = False
            for ligne in range(l_max):
                colonne = 0
                for colonne in range(c_max):
                    
                    Nord = ligne-1
                    Sud = ligne+1
                    Est = colonne+1
                    Ouest = colonne-1
                    
                    if calque.getVal(ligne,colonne)!=0:
                        
                        if Nord >= 0 and matrice.getVal(ligne,colonne).passageNord(matrice.getVal(Nord,colonne)) and calque.getVal(Nord,colonne)==0:
                            
                            marquer = True
                            
                            valeur = calque.getVal(ligne,colonne)
                            calque.setVal(Nord,colonne,valeur+1)
                            
                            #print("Nord")
                        
                        if Ouest >= 0 and matrice.getVal(ligne,colonne).passageOuest(matrice.getVal(ligne,Ouest)) and calque.getVal(ligne,Ouest)==0:
                            
                            marquer = True
                            
                            valeur = calque.getVal( ligne, colonne)
                            calque.setVal( ligne, Ouest, valeur+1)
                            
                            #print("Ouest")
                        
                        if Sud < l_max and matrice.getVal(ligne,colonne).passageSud(matrice.getVal(Sud,colonne)) and calque.getVal(Sud,colonne)==0:
                            
                            marquer = True
                            
                            valeur = calque.getVal( ligne, colonne)
                            calque.setVal( Sud, colonne, valeur+1)
                            
                            #print("Sud")
                        

                        if Est < c_max and matrice.getVal(ligne,colonne).passageEst(matrice.getVal(ligne,Est)) and calque.getVal(ligne,Est)==0:
                            
                            marquer = True
                            
                            valeur = calque.getVal(ligne, colonne)
                            calque.setVal( ligne, Est, valeur+1)
                            
                            #print("Est")
        
        #print(getVal(calque,ligA,colA))
        #print(getVal(calque,ligD,colD))
        
        if calque.getVal(ligA,colA)==0 or calque.getVal(ligD,colD)==0:
            return None
        
        liste_coords = [(ligA,colA)]
        
        # On part de l'arrivée et on remonte jusqu'à la case 1, on ne peut pas le faire dans l'autre sens !
        
        while liste_coords[-1]!=(ligD,colD):
            ligne = liste_coords[-1][0]
            colonne = liste_coords[-1][1]
            valeur = calque.getVal(ligne, colonne)
            
            Nord = ligne -1
            Sud = ligne +1
            Ouest = colonne -1
            Est = colonne +1
            
            if Nord >= 0 and calque.getVal( Nord, colonne)==valeur-1 and matrice.getVal(ligne, colonne).passageNord(matrice.getVal( Nord, colonne)):
                liste_coords.append((Nord, colonne))
            
            else:
                if Sud < 7 and calque.getVal( Sud, colonne)==valeur-1 and matrice.getVal(ligne,colonne).passageSud(matrice.getVal( Sud, colonne)):
                    liste_coords.append((Sud, colonne))
                
                else:
                    if Ouest >= 0 and calque.getVal( ligne, Ouest)==valeur -1 and matrice.getVal(ligne,colonne).passageOuest( matrice.getVal( ligne, Ouest)):
                        liste_coords.append((ligne, Ouest))
                    
                    else:
                        if Est < 7 and calque.getVal( ligne, Est)==valeur -1 and matrice.getVal(ligne,colonne).passageEst(matrice.getVal( ligne, Est)):
                            liste_coords.append((ligne, Est))
                        
                        else:
                            
                            print("Erreur Dramatique qui ne devrait jamais arriver!")
                            print("Contacter d'urgence le 06 41 24 11 20 si c'est le Cas!")
                            print("Ceci est un message sérieux! Bien que conservant un caractère humouristique")
                            print("Mais vraiment si le programme entre dans ce else, on est mal jeunes gens!")
        #print((ligD,colD,ligA,colA))
        #print(liste_coords)
        liste_coords.reverse()
        return liste_coords

    def getNbTresorsPlateau(self):
        """Fonction qui retourne le nombre de trésors restant sur le plateau"""
        nbTresor = 0
        for elem in self.Matrice:
            if elem.getTresor() == 0:
                nbTresor += 1
        if self.carteAjouer.getTresor() != 0:
            nbTresor +=1
        return nbTresor

    def setCarteAJouer(self, carte):
        """
        Attention, cette fonction détruit la carte à jouer précédente,
            Elle ne doit être appelée que dans la fonction JouerCarte de labyrinthe
        """
        self.carteAjouer = carte


#!/usr/bin/python

#-*- coding: utf-8 -*-

"""
la liste des caractères semi-graphiques correspondant aux différentes cartes
l'indice du caractère dans la liste correspond au codage des murs sur la carte
le caractère 'Ø' indique que l'indice ne correspond pas à une carte
"""
listeCartes=['╬','╦','╣','╗','╩','═','╝','Ø','╠','╔','║','Ø','╚','Ø','Ø','Ø']
# Numerotation des case de la liste de 0 a 15, on peut donc le coder avec cadre booleen comme suit :
# index 0 == 0000, index 1 == 0001, index 2 == 0010, index 3 == 0011 etc jusque index 15 == 1111

class CarteOO:
    
    def __init__(self,nord,est,sud,ouest,pions,tresor=0):
        self.nord = nord 
        self.est = est
        self.sud = sud
        self.ouest = ouest
        self.tresor = tresor
        self.pions = []

    def estValide(self):
        nb_murs=0
        if self.nord:
            nb_murs+=1
        if self.est:
            nb_murs+=1
        if self.ouest:
            nb_murs+=1
        if self.sud:
            nb_murs+=1
        if res<3:
            return True 
        return False

    def murNord(self):
        if self.nord:
            return True
        return False
        

    def murSud(self):
        if self.sud:
            return True
        return False

    def murEst(self):
        if self.est:
            return True
        return False

    def murOuest(self):
        if self.ouest:
            return True
        return False

    def getListePions(self):
        return self.pions

    def setListePion(self,liste_pions):
        self.pions=liste_pions

    def getNbPions(self):
        return len(self.getNbListePion())

    def possedePion(self, num):
        if num in self.getListePions():
            return True
        return False

    def getTresor(self):
        return self.tresor

    def prendreTresor(self):
        tresor = self.getTresor()
        self.tresor=0
        return tresor

    def mettreTresor(self, tresor):
        
        self.tresor= tresor
        return self.tresor

    def prendrePion(self, pion):
        if pion in self.pions:
            self.pions.remove(pion)

    def poserPion(self,pion):
        if pion not in self.pions:
            self.pions.append(pion)
            
    def tournerHoraire(self):
        tmp = self.nord
        self.nord=self.ouest
        self.ouest = self.sud
        self.sud = self.est
        self.est = tmp

    def tournerAntiHoraire(self):
        self.tournerHoraire()
        self.tournerHoraire()
        self.tournerHoraire()
        
    def tournerAleatoire(self):
        nb_tour = random.randint(0,3)
        i = 0
        while i <= nb_tour:
            self.tournerHoraire()
            i += 1

    def coderMurs(self):
        res = 0
        if self.murNord():
            res += 1
        if self.murEst():
            res += 2
        if self.murSud():
            res += 4
        if self.murOuest():
            res += 8
        return res

    def decoderMurs(self, code):
        if code-8 >= 0:
            self.ouest=True
            code = code-8
        else:
            self.ouest=False

        if code-4 >= 0:
            self.sud=True
            code = code-4
        else:
            self.sud=False

        if code-2 >= 0:
            self.est=True
            code = code-2
        else:
            self.est=False

        if code-1 >= 0:
            self.nord=True
            code = code-1
        else:
            self.nord=False


    def toChar(self):
        indice = self.coderMurs()
        return listeCartes[indice]

    def passageNord(self, carte2 ):
        if self.nord==False and carte2.sud==False:
            return True
        return False

    def passageSud(self, carte2):
        if self.sud==False and carte2.nord==False:
            return True
        return False

    def passageOuest(self, carte2):
        if self.ouest==False and carte2.est==False:
            return True
        return False

    def passageEst(self, carte2):
        if self.est==False and carte2.ouest==False:
            return True
        return False


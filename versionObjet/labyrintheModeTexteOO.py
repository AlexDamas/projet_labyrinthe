#! /usr/bin/python3
# -*- coding: utf-8 -*-
"""
                           Projet Labyrinthe 
        Projet Python 2019-2020 de 1ere année et AS DUT Informatique Orléans
        
   Programme principal du labyrinthe en mode texte
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""

from LabyrintheOO import *
from ansiColor import *
import sys
import time

class LabyrintheModeTexte(object):
    def __init__(self):
        labyrinthe=None
        
    def setLabyrinthe(self,labyrinthe):
        self.labyrinthe=labyrinthe
        
    def afficheCarte(self, carte,pion=1,tresor=-1):
            coulFond=NORMAL
            coulCar=NORMAL
            style=AUCUN
            if carte.getTresor()==tresor:
                coulFond=GRIS
                coulCar=NOIR
            lesPions=carte.getListePions()
            if len(lesPions)>0:
                if len(lesPions)>1:
                    style=GRAS
                if carte.possedePion(pion):
                    coulCar=pion
                else:  
                    coulCar=lesPions[0]
            pcouleur(carte.toChar(),coulCar,coulFond,style)
     
    def afficheLabyrinthe(self,message="",sauts=0):
        clearscreen();
        print(message)
        print('Cartes restantes :',end='')
        for i in range(1,self.labyrinthe.getNbParticipants()+1):
        #for i in range(0,self.labyrinthe.getNbParticipants()):
            pcouleur(self.labyrinthe.getListeJoueurs().nomJoueur(i).ljust(10)+' '+str(self.labyrinthe.ListeJoueurs.nbTresorsRestantsJoueur(i))+' ',i)
        print()
        print("C'est au tour de ",end='')
        pcouleur(self.labyrinthe.getNomJoueurCourant()+ " de jouer",self.labyrinthe.getNumJoueurCourant())
        print()
        tresor=self.labyrinthe.getTresorCourant()
        print("Le trésor recherché est :",tresor, "caché ",end='')
        coord=self.labyrinthe.getCoordonneesTresorCourant()
        if coord==None:
            print("sur la carte à jouer")
        else:
            print("en",coord)
        print()
        print(' ',sep='',end='')
        plateau=self.labyrinthe.plateau
        remplissage=' '*30
        print(remplissage,end='')
        for i in range(1,7,2):
            print(" "+str(i),sep='',end='')
        print()
        for i in range(plateau.getMatrice().getNbLignes()):
            print(remplissage,end='')            
            if i%2==0:
                print(' ',sep='',end='')
            else:
                print(str(i),sep='',end='')
            for j in range(plateau.getMatrice().getNbColonnes()):
                
                self.afficheCarte(plateau.getMatrice().getVal(i,j),self.labyrinthe.getNumJoueurCourant,tresor)
            if i%2==0:
                print(' ',sep='',end='')
            else:
                print(str(i),sep='',end='')            
            print()
        print(' ',sep='',end='')
        print(remplissage,end='')        
        for i in range(1,7,2):
            print(" "+str(i),sep='',end='')
        print()
        print("Carte à jouer: ",end='')
        self.afficheCarte(self.labyrinthe.getCarteAJouer(),tresor)
        for i in range(sauts):
            print()
        print()

    def animationChemin(self,chemin, joueur,pause=0.1):
        (xp,yp)=chemin.pop(0)
        for (x,y) in chemin:
            self.labyrinthe.getMatrice().getVal(xp,yp).prendrePion(joueur)
            self.labyrinthe.getMatrice().getVal(x,y).poserPion(joueur)
            self.afficheLabyrinthe(sauts=1)
            time.sleep(pause)
            xp,yp=x,y
        return xp,yp

    def saisirOrdre(self):
        
        while True:
            
            x = input("Entrez T pour tourner la carte dans le sens horaire"+"\n"+"Entrez N pour insérer la carte par le haut"+"\n"+"Entrez S pour insérer la carte par le bas"+"\n"+"Entrez E pour insérer la carte par la droite"+"\n"+"Entrez O pour insérer la carte par la gauche : "+"\n")
            x = x.upper()
            
            #print(x)
            
            if x in {'T','N','O','E','S'}:
                #print("Erreur:x")
                #return (-1,-1)
                if x == 'T':
                    return (x,None)
                else:
                    y = input("Entrez la rangée [1,3,5]:")
                    try:
                        y = int(y)
                    except:
                        y = None
                    
                    if y in {1,3,5}:

                        if not self.labyrinthe.coupInterdit( x, y):
                            return (x,int(y))
                        else:
                            print("C'est un Coup Interdit /!\\")
                            #return (-1,-1)

                    else:
                        print("Erreur:y")
                        #return (-1,-1)




    def saisirDeplacement(self):

        while True:
            
            try :
                
                lin = int(input("Entrez la ligne visée :"))
                col = int(input("Entrez la colonne visée :"))
                
                if int(lin) >= 0 and int(lin) < 7:
                    if int(col) >= 0 and int(col) < 7:
                        lin = int(lin)
                        col = int(col)
                        return (lin,col)
        
            except ValueError:
                
                print("Erreur dans les valeurs entrées, il faut des entiers : ")
            

    def main(self):
        print("Bienvenue dans le jeu du labyrinthe")
        nbJoueurs=input("Combien de joueurs? ")
        while nbJoueurs not in ['2','3','4']:
            print("Le nombre de joueurs doit être compris entre 2 et 4")
            nbJoueurs=input("Combien de joueurs? ")
        listeJoueurs = []
        for i in range(int(nbJoueurs)):
            nom = input("Nom du joueur"+ str(i+1)+"?")
            listeJoueurs.append(nom)
            
        nbTresors = input("Combien de trésor à trouver par joueur (0 pour le maximun possible)")
        
        try:
            nbTresorsInt=int(nbTresors)
        except:
            nbTresorsInt=0
            print("Le nombre maximum de trésors a été choisi")
        l=Labyrinthe(listeJoueurs,nbTresorsMax = nbTresorsInt)
        self.setLabyrinthe(l)
        self.afficheLabyrinthe()
    
        fini=False
        while not fini:
            finOrdre=False
            while(not finOrdre):
                ordre=self.saisirOrdre()
                if str(ordre[0]) in 'tT':
                    self.labyrinthe.tournerCarte()
                    self.afficheLabyrinthe("La carte a été tournée")
                elif str(ordre[0]) in 'qQ':
                    print("Abandon de la partie!")
                    sys.exit()
                else:
                    self.labyrinthe.jouerCarte(ordre[0].upper(),int(ordre[1]))
                    self.afficheLabyrinthe("La carte a été insérée en "+ordre[0].upper()+" "+str(ordre[1]))                
                    finOrdre=True
            
            chemin=None
            while chemin==None:
                xA,yA=self.saisirDeplacement()
                chemin=self.labyrinthe.accessibleDistJoueurCourant(xA,yA)
                if chemin == None:
                    print("Vous ne pouvez pas atteindre cette case")
            #chemin= self.saisirDeplacementAnime()
            jc=self.labyrinthe.getNumJoueurCourant()
            xA,yA=self.animationChemin(chemin,jc)
                    
                
            c=self.labyrinthe.getMatrice().getVal(xA,yA)
            t=self.labyrinthe.getTresorCourant()
            message=""
                
            if c.getTresor()==t:
                c.prendreTresor()
                self.labyrinthe.getListeJoueurs().joueurCourantTrouveTresor()
                #if self.labyrinthe.getListeJoueurs().joueurCourantTrouveTresor()==0:
                if self.labyrinthe.getListeJoueurs().getJoueurCourant().getNbTresor()==0:
                    message=self.labyrinthe.getListeJoueurs().nomJoueurCourant()+" a gagné"
                    fini=True
                else:
                    message=self.labyrinthe.getListeJoueurs().nomJoueurCourant()+" vient de trouver le trésor "+str(t)
            self.labyrinthe.getListeJoueurs().changerJoueurCourant()
            self.afficheLabyrinthe(message)
    
        print("Merci au revoir")
    
g=LabyrintheModeTexte()
g.main()

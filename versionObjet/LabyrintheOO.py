#!/usr/bin/python
#-*- coding: utf-8 -*-

from ListeDeJoueurOO import *
from PlateauOO import *

class Labyrinthe:
    #Apparement le programme principal veut nbdejoueur et non par nom de joueur, étrange..
    def __init__(self, nomjoueurs,nbTresors=24, nbTresorsMax=0):
        i = 0
        Liste_joueur = []
        while i < len(nomjoueurs):
            Liste_joueur.append(nomjoueurs[i])
            i+=1
        self.ListeJoueurs = ListeDeJoueurOO(Liste_joueur)
        self.nbTresors = nbTresors
        self.nbTresorsMax = nbTresorsMax
        self.ListeJoueurs.distribuerTresors(nbTresors, nbTresorsMax)
        self.ListeJoueurs.initAleatoireJoueurCourant()
        
        nbJoueur = self.ListeJoueurs.getNbJoueurs()
        self.plateau = PlateauOO(nbJoueur,nbTresors)
        self.phase = 1
        self.direction = None
        self.rangee = None
        

    def getPlateau(self):
        """
        retourne la matrice représentant le plateau de jeu
            paramètre: labyrinthe le labyrinthe considéré
            résultat: la matrice représentant le plateau de ce labyrinthe
        """
        return self.plateau

    def getMatrice(self):
        
        return self.getPlateau().Matrice

    def getNbParticipants(self):
        """
        retourne le nombre de joueurs engagés dans la partie
            paramètre: labyrinthe le labyrinthe considéré
            résultat: le nombre de joueurs de la partie
        """
        return self.ListeJoueurs.getNbJoueurs()

    def getNomJoueurCourant(self):
        """
        retourne le nom du joueur courant
            paramètre: labyrinthe le labyrinthe considéré
            résultat: le nom du joueurs courant
        """
        return self.ListeJoueurs.getJoueurCourant().getNom()

    def getNumJoueurCourant(self):
        """
        retourne le numero du joueur courant
            paramètre: labyrinthe le labyrinthe considéré
            résultat: le numero du joueurs courant
        """
        return self.ListeJoueurs.numJoueurCourant()

    def getPhase(self):
        """
        retourne la phase du jeu courante
            paramètre: labyrinthe le labyrinthe considéré
            résultat: le numéro de la phase de jeu courante
        """
        return self.phase

    def changerPhase(self):
        """
        change de phase de jeu en passant la suivante
            paramètre: labyrinthe le labyrinthe considéré
            la fonction ne retourne rien mais modifie le labyrinthe
        """
        if self.phase == 1 :
            self.phase = 2
        else:
            if self.phase == 2:
                self.phase = 1
            else:
                print("Erreur critique dans la gestion des phases")

    def getNbTresors(self):
        """
        retourne le nombre de trésors qu'il reste sur le labyrinthe
            paramètre: labyrinthe le labyrinthe considéré
            résultat: le nombre de trésors sur le plateau
        """
        return self.plateau.getNbTresorsPlateau()

    def getListeJoueurs(self):
        """
        retourne la liste joueur structures qui gèrent les joueurs et leurs trésors a trouver
            paramètre: labyrinthe le labyrinthe considéré
            résultat: les joueurs sous la forme de la structure implémentée dans listeJoueurs.py
        """
        return self.ListeJoueurs

    def enleverTresor(self, lin, col, numTresor):
        """
        enleve le trésor numTresor du plateau du labyrinthe. 
            Si l'opération s'est bien passée le nombre total de trésors dans le labyrinthe
            est diminué de 1
            paramètres: labyrinthe: le labyrinthe considéré
                        lig: la ligne où se trouve la carte
                        col: la colonne où se trouve la carte
                        numTresor: le numéro du trésor à prendre sur la carte
            la fonction ne retourne rien mais modifie le labyrinthe
        """
        self.plateau.prendreTresorPlateau(lin,col,numTresor)

    def prendreJoueurCourant(self, lin, col):
        """
        enlève le joueur courant de la carte qui se trouve sur la case lin,col du plateau
            si le joueur ne s'y trouve pas la fonction ne fait rien
            paramètres: labyrinthe: le labyrinthe considéré
                        lig: la ligne où se trouve la carte
                        col: la colonne où se trouve la carte
            la fonction ne retourne rien mais modifie le labyrinthe
        """
        self.plateau.prendrePionPlateau(lin,col, self.ListeJoueurs.numJoueurCourant())

    def poserJoueurCourant(self, lin, col):
        """
        pose le joueur courant sur la case lin,col du plateau
            paramètres: labyrinthe: le labyrinthe considéré
                        lig: la ligne où se trouve la carte
                        col: la colonne où se trouve la carte
            la fonction ne retourne rien mais modifie le labyrinthe
        """
        self.plateau.poserPionPlateau(lin, col, self.ListeJoueurs.numJoueurCourant())

    def getCarteAJouer(self):
        """
        donne la carte à jouer
            paramètre: labyrinthe: le labyrinthe considéré
            résultat: la carte à jouer
        """
        return self.plateau.carteAjouer

    def coupInterdit(self, direction, rangee):
        """
        retourne True si le coup proposé correspond au coup interdit
            elle retourne False sinon
            paramètres: labyrinthe: le labyrinthe considéré
                        direction: un caractère qui indique la direction choisie ('N','S','E','O')
                        rangee: le numéro de la ligne ou de la colonne choisie
            résultat: un booléen indiquant si le coup est interdit ou non
        """
        (direc_prec, rangee_prec) = self.getCoupPrecedent()
    
        if (direc_prec,rangee_prec) == (None,None):
            return False
        else:
            if rangee_prec == rangee:
                if (direc_prec == 'N' and direction == 'S') or (direc_prec == 'S' and direction == 'N'):
                    return True
                if (direc_prec == 'O' and direction == 'E') or (direc_prec == 'E' and direction == 'O'):
                    return True
            else:
                return False
        
    def getCoupPrecedent(self):
        """
        Récupère les info du coup précédent
        """
        return (self.direction, self.rangee)

    def setCoupPrecedent(self, direction, rangee):
        """
        Met dans labyrinthe la valeur du coup precédent
        """
    
        self.direction = direction
        self.rangee = direction
    

    def jouerCarte(self, direction, rangee):
        """
        fonction qui joue la carte amovible dans la direction et sur la rangée passées 
            en paramètres. Cette fonction
               - met à jour le plateau du labyrinthe
               - met à jour la carte à jouer
               - met à jour la nouvelle direction interdite
            paramètres: labyrinthe: le labyrinthe considéré
                        direction: un caractère qui indique la direction choisie ('N','S','E','O')
                        rangee: le numéro de la ligne ou de la colonne choisie
            Cette fonction ne retourne pas de résultat mais mais à jour le labyrinthe
        """
        if direction == 'N':
            carte = self.getMatrice().decalageColonneEnBas(rangee, self.getCarteAJouer())
            if (carte.getListePions() != []):
                
                pions = carte.getListePions()
                for elem in pions:
                    self.getPlateau().poserPionPlateau(0, rangee, elem)
                for elem in pions:
                    carte.setListePion([])
        
        if direction == 'S':
            carte = self.getMatrice().decalageColonneEnHaut(rangee, self.getCarteAJouer())
            if (carte.getListePions() != []):
                
                pions = carte.getListePions()
                for elem in pions:
                    self.getPlateau().poserPionPlateau(6, rangee, elem)
                for elem in pions:
                    carte.setListePion([])


        
        if direction == 'E':
            
            carte = self.getMatrice().decalageLigneAGauche(rangee, self.getCarteAJouer())
            if (carte.getListePions() != []):
                
                pions = carte.getListePions()
                for elem in pions:
                    self.getPlateau().poserPionPlateau( rangee,6,  elem)
                for elem in pions:
                    carte.setListePion([])

        if direction == 'O':
            carte = self.getMatrice().decalageLigneADroite(rangee, self.getCarteAJouer())
            if (carte.getListePions() != []):
                
                pions = carte.getListePions()
                for elem in pions:
                    self.getPlateau().poserPionPlateau(rangee,0, elem)
                for elem in pions:
                    carte.setListePion([])
        
        self.getPlateau().setCarteAJouer(carte)
        self.setCoupPrecedent(direction, rangee)

    def tournerCarte(self, sens="H"):
        """
        tourne la carte à jouer dans le sens indiqué en paramètre (H horaire A antihoraire)
            paramètres: labyrinthe: le labyrinthe considéré
                        sens: un caractère indiquant le sens dans lequel tourner la carte
             Cette fonction ne retourne pas de résultat mais mais à jour le labyrinthe
        """
        if sens == 'H':
            self.getCarteAJouer().tournerHoraire()
        if sens == 'A': 
            self.getCarteAJouer().tournerAntiHoraire()

    def getTresorCourant(self):
        """
        retourne le numéro du trésor que doit cherche le joueur courant
            paramètre: labyrinthe: le labyrinthe considéré 
            resultat: le numéro du trésor recherché par le joueur courant
        """
        return self.ListeJoueurs.prochaineTresorJoueur(self.getNumJoueurCourant())

    def getCoordonneesTresorCourant(self):
        """
        donne les coordonnées du trésor que le joueur courant doit trouver
            paramètre: labyrinthe: le labyrinthe considéré 
            resultat: les coordonnées du trésor à chercher ou None si celui-ci 
                      n'est pas sur le plateau
        """
        return self.getPlateau().getCoordonneesTresor(self.getTresorCourant())

    def getCoordonneesJoueurCourant(self):
        """
        donne les coordonnées du joueur courant sur le plateau
            paramètre: labyrinthe: le labyrinthe considéré 
            resultat: les coordonnées du joueur courant ou None si celui-ci 
                      n'est pas sur le plateau
        """
        #print("Num =="+str(self.getNumJoueurCourant()))
        return self.getPlateau().getCoordonneesJoueur(self.getNumJoueurCourant())

    def executerActionPhase1(self, action, rangee):
        """
        exécute une action de jeu de la phase 1
            paramètres: labyrinthe: le labyrinthe considéré
                        action: un caractère indiquant l'action à effecter
                                si action vaut 'T' => faire tourner la carte à jouer
                                si action est une des lettres N E S O et rangee est un des chiffre 1,3,5 
                                => insèrer la carte à jouer à la direction action sur la rangée rangee
                                   et faire le nécessaire pour passer en phase 2
            résultat: un entier qui vaut
                      0 si l'action demandée était valide et demandait de tourner la carte
                      1 si l'action demandée était valide et demandait d'insérer la carte
                      2 si l'action est interdite car l'opposée de l'action précédente
                      3 si action et rangee sont des entiers positifs
                      4 dans tous les autres cas
        """
        lettres_valides = {'N','O','E','S'}
        rangees_valides = {1,3,5}
        
        #print("Valeur rangee ; ",rangee)
        #print("Action Vaut ; ", action)
        if (action != -1 and (rangee == None or rangee in rangees_valides)):
            # Les inputs ont la bonne taille pour être valide

            
            action = action.upper()
            
            if action == 'T':
                self.tournerCarte()
                return 0
            if action in lettres_valides and rangee in rangees_valides:
                
                if self.coupInterdit(action, rangee):
                    return 2
                else:
                    self.jouerCarte(action, rangee)
                    self.changerPhase()
                    return 1
            else:
                if action.isnumeric() and rangee.isnumeric():
                    return 3
                else:
                    return 4
        else:
            # Un des inputs (ou les deux) font plus qu'un caractère
            #print("plop")
            return 4

    def accessibleDistJoueurCourant(self, ligA, colA):
        """
        verifie si le joueur courant peut accéder la case ligA,colA
            si c'est le cas la fonction retourne une liste représentant un chemin possible
            sinon ce n'est pas le cas, la fonction retourne None
            paramètres: labyrinthe le labyrinthe considéré
                        ligA la ligne de la case d'arrivée
                        colA la colonne de la case d'arrivée
            résultat: une liste de couples d'entier représentant un chemin que le joueur
                      courant atteigne la case d'arrivée s'il existe None si pas de chemin
        """
        #print(self.getCoordonneesJoueurCourant())
        (ligD,colD) = self.getCoordonneesJoueurCourant()
        return self.getPlateau().accessibleDist( ligD, colD, ligA, colA)

    def finirTour(self):
        """
        vérifie si le joueur courant vient de trouver un trésor (si oui fait le nécessaire)
            vérifie si la partie est terminée, si ce n'est pas le cas passe au joueur suivant
            paramètre: labyrinthe le labyrinthe considéré
            résultat: un entier qui vaut
                      0 si le joueur courant n'a pas trouvé de trésor
                      1 si le joueur courant a trouvé un trésor mais la partie n'est pas terminée
                      2 si le joueur courant a trouvé son dernier trésor (la partie est donc terminée)
        """
        (i,j) = self.getCoordonneesJoueurCourant()
        if (i,j) == self.getCoordonneesTresorCourant():
            
            self.enleverTresor(i, j, self.getTresorCourant())
            
            
            
            self.getListeJoueurs().joueurCourantTrouveTresor()
            
            if self.getListeJoueurs().joueurCourantAFini():
                return 2
            else:
                self.changerPhase()
                self.getListeJoueurs().changerJoueurCourant()
                return 1
            
        else:
            #print(getNomJoueurCourant(labyrinthe))
            self.getListeJoueurs().changerJoueurCourant()
            #print(getNomJoueurCourant(labyrinthe))
            self.changerPhase()
            return 0


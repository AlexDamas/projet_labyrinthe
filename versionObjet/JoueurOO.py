#!/usr/bin/python
#-*- coding: utf-8 -*-

class JoueurOO:
    def __init__(self, nom):
        self.Nom = nom
        self.ListeDeTresors = []

    def AjouteTresor(self, tresor):
        """
        ajoute un trésor à trouver à un joueur (ce trésor sera ajouter en fin de liste) Si le trésor est déjà dans la liste des trésors à trouver la fonction ne fait rien
            paramètres:
                joueur le joueur à modifier
                tresor un entier strictement positif
            la fonction ne retourne rien mais modifie le joueur
        """
        if tresor not in self.ListeDeTresors:
            self.ListeDeTresors.append(tresor)
        

    def prochainTresor(self):
        """
        retourne le prochain trésor à trouver d'un joueur, retourne None si aucun trésor n'est à trouver
            paramètre:
                joueur le joueur
            résultat un entier représentant le trésor ou None
        """
        # Retourne le premier trésor de la liste
        if self.getNbTresor() > 0:
            #Il y a au moin un tresor
            return self.ListeDeTresors[0]
        return None

    def tresorTrouve(self):
        """
        enleve le premier trésor à trouver car le joueur l'a trouvé
            paramètre:
                joueur le joueur
            la fonction ne retourne rien mais modifie le joueur
        """
        self.ListeDeTresors.pop(0)

    def getNbTresor(self):
        """
        retourne le nombre de trésors qui reste à trouver
            paramètre: joueur le joueur
            résultat: le nombre de trésors attribués au joueur
        """
        # Retourne un entier représentant le nombre de trésors
        return len(self.ListeDeTresors)

    def getNom(self):
        """
        retourne le nom du joueur
            paramètre: joueur le joueur
            résultat: le nom du joueur
        """
        return self.Nom

if __name__ == "__main__":
    
    TestJoueur = JoueurOO("Bob")
    TestJoueur.AjouteTresor(3)
    TestJoueur.AjouteTresor(5)
    assert TestJoueur.prochainTresor() == 3
    TestJoueur.tresorTrouve()
    assert TestJoueur.prochainTresor() == 5
    assert TestJoueur.getNom()=="Bob"
    assert TestJoueur.getNbTresor() == 1
    

def plateau(nbJoueurs, nbTresors):
    
    defaultcard = Carte(True,True,True,True,0,[])
    plate = Matrice(7,7,defaultcard)
    tresor_debut=1
    liste_tresor=random.sample(range(1,nbTresors+1),nbTresors)
    for i in range(getNbLignes(plate):
        for j in range(getNbColonnes(plate):
            if i%2==0 and j%2==0:
                if i==0:
                    if j==0:
                        setVal(plate,i,j,Carte(True,False,False,True,0,[]))
                        poserPion(plate['Matrice'][i][j],1)
                    if j==2 or j==4:
                        setVal(plate,i,j,Carte(True,False,False,False,0,[]))
                        if liste_tresor != []:
                            mettreTresor(plate['Matrice'][i][j],liste_tresor[0])
                            liste_tresor.pop(0)
                    if j==6:
                        setVal(plate,i,j,Carte(True,True,False,False,0,[]))
                        if nbJoueurs==3:
                            poserPion(plate['Matrice'][i][j],3)
                else:
                    if i==6:
                        if j==0:
                            setVal(plate,i,j,Carte(False,False,True,True,0,[]))
                            if nbJoueurs==4:
                                poserPion(plate['Matrice'][i][j],4)
                        if j==2 or j==4:
                            setVal(plate,6,2,Carte(False,False,True,False,0,[]))
                            if liste_tresor != []:
                                mettreTresor(plate['Matrice'][i][j],liste_tresor[0])
                                liste_tresor.pop(0)
                        if j==6:
                            setVal(plate,6,6,Carte(False,True,True,False,0,[]))
                            if nbJoueurs==2:
                                poserPion(plate['Matrice'][i][j],2)
                    else:
                        if i==2:
                            if j==0 or j==2:
                                setVal(plate,i,j,Carte(False,False,False,True,0,[]))
                                if liste_tresor != []:
                                    mettreTresor(plate['Matrice'][i][j],liste_tresor[0])
                                    liste_tresor.pop(0)
                            else:
                                if j==4:
                                    setVal(plate,i,j,Carte(True,False,False,False,0,[]))
                                else:
                                    if j==6:
                                        setVal(plate,i,j,Carte(False,False,True,False,0,[]))
                                

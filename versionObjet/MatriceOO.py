#!/usr/bin/python
#-*- coding: utf-8 -*-

class MatriceOO:
    def __init__(self,nbLignes,nbColonnes, valeurParDefaut=0):
        self._nbLignes = nbLignes
        self._nbColonnes = nbColonnes
        self._listeDesValeurs = [valeurParDefaut]*(nbLignes*nbColonnes)

    def getNbLignes(self):
        """
        retourne le nombre de lignes de la matrice
            paramètre: matrice la matrice considérée
        """
        return self._nbLignes

    def getNbColonnes(self):
        """
        retourne le nombre de colonnes de la matrice
            paramètre: matrice la matrice considérée
        """
        return self._nbColonnes

    def getVal(self, ligne, colonne):
        """
        retourne la valeur qui se trouve en (ligne,colonne) dans la matrice
            paramètres: matrice la matrice considérée
                        ligne le numéro de la ligne (en commençant par 0)
                        colonne le numéro de la colonne (en commençant par 0)
        """
        return self._listeDesValeurs[ligne*self._nbColonnes+colonne]

    def setVal(self, ligne, colonne, valeur):
        """
        met la valeur dans la case se trouve en (ligne,colonne) de la matrice
            paramètres: matrice la matrice considérée
                        ligne le numéro de la ligne (en commençant par 0)
                        colonne le numéro de la colonne (en commençant par 0)
                        valeur la valeur à stocker dans la matrice
            cette fonction ne retourne rien mais modifie la matrice
        """
        self._listeDesValeurs[ligne*self._nbColonnes+colonne] = valeur


    def decalageLigneAGauche(self, numLig, nouvelleValeur=0):
        """
        permet de décaler une ligne vers la gauche en insérant une nouvelle
            valeur pour remplacer la premiere case à droite de cette ligne
            le fonction retourne la valeur qui a été éjectée
            paramèteres: matrice la matrice considérée
                         numLig le numéro de la ligne à décaler
                         nouvelleValeur la valeur à placer
            résultat la valeur qui a été ejectée lors du décalage
        """

        longueur_liste = self.getNbColonnes()
        ligne_valeur = []
        for valeur in range(longueur_liste):
            ligne_valeur.append(self.getVal(numLig, valeur))

        res = ligne_valeur[0]
        valeur = 0
        for i in range(1,longueur_liste):
            ligne_valeur[i-1] = ligne_valeur[i]

        ligne_valeur[-1] = nouvelleValeur

        i = 0
        for i in range(0,longueur_liste):
            self.setVal(numLig,i,ligne_valeur[i])

        return res

    def decalageLigneADroite(self, numLig, nouvelleValeur=0):
        """
        decale la ligne numLig d'une case vers la droite en insérant une nouvelle
            valeur pour remplacer la premiere case à gauche de cette ligne
            paramèteres: matrice la matrice considérée
                         numLig le numéro de la ligne à décaler
                         nouvelleValeur la valeur à placer
            résultat: la valeur de la case "ejectée" par le décalage
        """
        longueur_liste = self.getNbColonnes()
        ligne_valeur = []

        for valeur in range(longueur_liste):
            ligne_valeur.append(self.getVal(numLig, valeur))

        res = ligne_valeur[-1]

        ligne_valeur.insert(0,nouvelleValeur)
        ligne_valeur.pop()

        for i in range(0,longueur_liste):
            self.setVal(numLig,i,ligne_valeur[i])

        return res



    def decalageColonneEnHaut(self, numCol, nouvelleValeur):
        """
        decale la colonne numCol d'une case vers le haut en insérant une nouvelle
            valeur pour remplacer la premiere case en bas de cette ligne
            paramèteres: matrice la matrice considérée
                         numCol le numéro de la colonne à décaler
                         nouvelleValeur la valeur à placer
            résultat: la valeur de la case "ejectée" par le décalage
        """
        longueur_liste = self.getNbLignes()
        ligne_valeur = []

        for valeur in range(longueur_liste):
            ligne_valeur.append(self.getVal(valeur,numCol))
        
        res = ligne_valeur[0]

        for i in range(1,longueur_liste):
            ligne_valeur[i-1] = ligne_valeur[i]

        ligne_valeur[-1] = nouvelleValeur

        for i in range(0,longueur_liste):
            self.setVal(i ,numCol,ligne_valeur[i])


        return res


    def decalageColonneEnBas(self, numCol, nouvelleValeur):
        """
        decale la colonne numCol d'une case vers le bas en insérant une nouvelle
            valeur pour remplacer la premiere case en haut de cette ligne
            paramèteres: matrice la matrice considérée
                         numCol le numéro de la colonne à décaler
                         nouvelleValeur la valeur à placer
            résultat: la valeur de la case "ejectée" par le décalage
        """
        longueur_liste = self.getNbLignes()
        ligne_valeur = []

        for valeur in range(longueur_liste):
            ligne_valeur.append(self.getVal(valeur,numCol))
        
        res = ligne_valeur[-1]

        ligne_valeur.insert(0,nouvelleValeur)
        ligne_valeur.pop()

        for i in range(0,longueur_liste):
            self.setVal(i,numCol,ligne_valeur[i])

        return res

##################
### Pour tests ###
##################

    def afficheLigneSeparatrice(self,tailleCellule=4):
        print()
        for i in range(self.getNbColonnes()+1):
            print('-'*tailleCellule+'+',end='')
        print()

    def affiche(self,tailleCellule=4):
        nbColonnes=self.getNbColonnes()
        nbLignes=self.getNbLignes()
        print(' '*tailleCellule+'|',end='')
        for i in range(nbColonnes):
            print(str(i).center(tailleCellule)+'|',end='')
        self.afficheLigneSeparatrice(tailleCellule)
        for i in range(nbLignes):
            print(str(i).rjust(tailleCellule)+'|',end='')
            for j in range(nbColonnes):
                print(str(self.getVal(i,j)).rjust(tailleCellule)+'|',end='')
            self.afficheLigneSeparatrice(tailleCellule)
        print()

if __name__ == '__main__':
    m = MatriceOO(3,3,0)
    m.affiche()
    m.decalageColonneEnBas(2,1)




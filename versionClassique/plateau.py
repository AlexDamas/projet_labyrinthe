# -*- coding: utf-8 -*-
"""
                           Projet Labyrinthe 
        Projet Python 2019-2020 de 1ere année et AS DUT Informatique Orléans
        
   Module plateau
   ~~~~~~~~~~~~~~
   
   Ce module gère le plateau de jeu. 
"""

from matrice import *
from carte import *

def Plateau(nbJoueurs, nbTresors):
    """
    créer un nouveau plateau contenant nbJoueurs et nbTrésors
    paramètres: nbJoueurs le nombre de joueurs (un nombre entre 1 et 4)
                nbTresors le nombre de trésor à placer (un nombre entre 1 et 49)
    resultat: un couple contenant
              - une matrice de taille 7x7 représentant un plateau de labyrinthe où les cartes
                ont été placée de manière aléatoire
              - la carte amovible qui n'a pas été placée sur le plateau
    """
    # Carte par défaut pour remplir la matrice du plateau
    defaultcard = Carte(True,True,True,True,0,[])
    plate = Matrice(7,7,defaultcard)
    tresor_debut=1 #dans labyrinthe.py, la fonction Labyrinthe me renvoyait une erreur "cette variable est référencée avant l'assignement". Je l'ai donc initialiser par défaut à 1
    #nbTresors = 1 #dans labyrinthe.py, la fonction Labyrinthe me renvoyait une erreur "cette variable est référencée avant l'assignement". Je l'ai donc initialiser par défaut à 1
    
    #
    # Construction des cases fixes :
    #
    
    setVal(plate,0,0,Carte(True,False,False,True,0,[]))
    setVal(plate,0,6,Carte(True,True,False,False,0,[]))
    setVal(plate,6,6,Carte(False,True,True,False,0,[]))
    setVal(plate,6,0,Carte(False,False,True,True,0,[]))
    
    setVal(plate,0,2,Carte(True,False,False,False,0,[]))
    setVal(plate,0,4,Carte(True,False,False,False,0,[]))
    setVal(plate,6,2,Carte(False,False,True,False,0,[]))
    setVal(plate,6,4,Carte(False,False,True,False,0,[]))
    
    setVal(plate,2,0,Carte(False,False,False,True,0,[]))
    setVal(plate,2,2,Carte(False,False,False,True,0,[]))
    setVal(plate,2,4,Carte(True,False,False,False,0,[]))
    setVal(plate,2,6,Carte(False,False,True,False,0,[]))
    
    setVal(plate,4,0,Carte(False,False,False,True,0,[]))
    setVal(plate,4,2,Carte(False,False,True,False,0,[]))
    setVal(plate,4,4,Carte(False,True,False,False,0,[]))
    setVal(plate,4,6,Carte(False,True,False,False,0,[]))
    
    # Positionnement des trésors sur les cartes fixes, le tuple (None,None) est un Teneur de Place moche je corrigerai ça plus tard
    
    liste_carte_fixes_avec_tresors = [(None,None),(0,2),(0,4),(2,0),(2,2),(2,4),(2,6),(4,0),(4,2),(4,4),(4,6),(6,2),(6,4)]
    
    for i in range(1,nbTresors+1):
        if i <= 12:
            coords = liste_carte_fixes_avec_tresors[i]
            mettreTresor(plate['Matrice'][coords[0]][coords[1]],i)
    
    # Je ne sais plus à quoi cela sert....
    
    if nbTresors <= 12:
        tresor_debut = 0
        nbTresors = 0
    else:
        tresor_debut = 13
    
    liste_amovible = creerCartesAmovibles(tresor_debut, nbTresors+1)
    indice_amovible = 0
    
    # Rempli les carte par défaut avec des cartes amovibles
    
    for i in range(7):
        j = 0
        for j in range(7):
            #print(coderMurs(plate['Matrice'][i][j]))
            
            # Ici == 15 fait référence au code de la carte par défaut
            if coderMurs(plate['Matrice'][i][j]) == 15:
                setVal(plate,i,j,liste_amovible[indice_amovible])
                indice_amovible+=1
    
    # Cette partie est factorisable! #YakaFokon
    
    if nbJoueurs >= 1:
        poserPion(plate['Matrice'][0][0],1)
    if nbJoueurs >= 2:
        poserPion(plate['Matrice'][6][6],2)
    if nbJoueurs >= 3:
        poserPion(plate['Matrice'][0][6],3)
    if nbJoueurs == 4:
        poserPion(plate['Matrice'][6][0],4)
    
    ## J'ai transformé plateau en liste plutôt que tuple pour pouvoir changer la valeur de la carte à jouer LOL! o.O #Rémy maitre batracien
    return [plate,liste_amovible[-1]]#retourne un tuple avec un dico il me semble. Est-ce normal? Ui :-}


def print_labyrinthe(plate):
    #
    # Double boucle de test pour afficher le rendu du labyrinthe en ascii
    # Peut être prévoir d'individualiser ce code dans une fonction car ça pourrait
    # resservir au débuggage ?
    #
    i = 0
    for i in range(7):
        j = 0
        line = ''
        for j in range(7):
            
            line += listeCartes[coderMurs(plate['Matrice'][i][j])]
        
        print(line)

def creerCartesAmovibles(tresorDebut,nbTresors):
    """
    fonction utilitaire qui permet de créer les cartes amovibles du jeu en y positionnant
    aléatoirement nbTresor trésors
    la fonction retourne la liste, mélangée aléatoirement, des cartes ainsi créées
    paramètres: tresorDebut: le numéro du premier trésor à créer
                nbTresors: le nombre total de trésor à créer
    résultat: la liste mélangée aléatoirement des cartes amovibles créees
    """
    # Pool initial de carte restante après "pose" des cartes fixes du plateau
    
    pool_carte = [1]*6
    carte_L = [3]*16
    carte_I = [5]*12
    pool_carte.extend(carte_L)
    pool_carte.extend(carte_I)
    
    pool_carte_melangees = random.sample(pool_carte, 34)
    
    #
    #Boucle permettant de "Tourner" les "cartes" de façon aléatoire
    #
    
    for indice in range(len(pool_carte_melangees)):
        
        if pool_carte_melangees[indice] == 1:
            c = random.sample({1,2,4,8},1)
        if pool_carte_melangees[indice] == 3:
            c = random.sample({3,6,9,12},1)
        if pool_carte_melangees[indice] == 5:
            c = random.sample({5,10},1)
        
        pool_carte_melangees[indice] = c[0]
    
    #
    # Création des dictionnaires de cartes à partir de chacun valeur d'indice
    #
    
    # Voir pour réunir le haut et le bas ici !
    
    liste_carte_melangees = []
    for indice in pool_carte_melangees:
        liste_carte_melangees.append(Carte(True,True,True,True,0,[]))
        decoderMurs(liste_carte_melangees[-1],indice)
        #print("\n###",listeCartes[coderMurs(liste_carte_melangees[-1])])
    
    #
    # Intégration des trésors dans la liste de carte
    #
    #print(tresorDebut)
    #print(nbTresors)
    liste_tresor = range(tresorDebut, nbTresors)
    liste_tresor = random.sample(liste_tresor, len(liste_tresor))
    #print(liste_tresor)
    
    positions_tresors = random.sample(range(34),len(liste_tresor))
    for indice in range(len(positions_tresors)):
        mettreTresor(liste_carte_melangees[positions_tresors[indice]],liste_tresor[indice])
    
    #print(liste_carte_melangees)
    return liste_carte_melangees

creerCartesAmovibles(0,12)

P = Plateau(4,12)
print_labyrinthe(P[0])
P0 = P[0]
#decalageLigneAGauche(P0,2,P[1])
#decalageLigneAGauche(P0,2,P[1])
#decalageLigneAGauche(P0,2,P[1])
#decalageLigneAGauche(P0,2,P[1])
#print_labyrinthe(P[0])



def prendreTresorPlateau(plateau,lig,col,numTresor):
    """
    prend le tresor numTresor qui se trouve sur la carte en lin,col du plateau
    retourne True si l'opération s'est bien passée (le trésor était vraiment sur
    la carte
    paramètres: plateau: le plateau considéré
                lig: la ligne où se trouve la carte
                col: la colonne où se trouve la carte
                numTresor: le numéro du trésor à prendre sur la carte
    resultat: un booléen indiquant si le trésor était bien sur la carte considérée
    """
    carte = getVal(plateau[0],lig,col)
    tresor = getTresor(carte)
    if tresor == 0:
        return False
    else:
        if tresor != numTresor:
            return False
        else:
            prendreTresor(carte)
            try:
                test = getTresor(carte)
                test == 0
            except:
                print("Erreur script pour enlever le tresor d'une carte du plateau")
            return True

print(prendreTresorPlateau(P,0,2,1))

def getCoordonneesTresor(plateau,numTresor):
    """
    retourne les coordonnées sous la forme (lig,col) du trésor passé en paramètre
    paramètres: plateau: le plateau considéré
                numTresor: le numéro du trésor à trouver
    resultat: un couple d'entier donnant les coordonnées du trésor ou None si
              le trésor n'est pas sur le plateau
    """
    #print(plateau[0])
    
    for i in range(7):
        j = 0
        for j in range(7):
            carte = getVal(plateau[0], i, j)
            if getTresor(carte) == numTresor:
                return (i,j)
    return None

assert getCoordonneesTresor(P,2)==(0,4)

def getCoordonneesJoueur(plateau,numJoueur):
    """
    retourne les coordonnées sous la forme (lig,col) du joueur passé en paramètre
    paramètres: plateau: le plateau considéré
                numJoueur: le numéro du joueur à trouver
    resultat: un couple d'entier donnant les coordonnées du joueur ou None si
              le joueur n'est pas sur le plateau
    """
    for i in range(7):
        j = 0
        for j in range(7):
            
            carte = getVal(plateau[0],i,j)
            ls_pion = getListePions(carte)
            if numJoueur in ls_pion:
                return (i,j)
    return None

print(getCoordonneesJoueur(P,2))

def prendrePionPlateau(plateau,lin,col,numJoueur):
    """
    prend le pion du joueur sur la carte qui se trouve en (lig,col) du plateau
    paramètres: plateau:le plateau considéré
                lin: numéro de la ligne où se trouve le pion
                col: numéro de la colonne où se trouve le pion
                numJoueur: le numéro du joueur qui correspond au pion
    Cette fonction ne retourne rien mais elle modifie le plateau
    """
    carte = getVal(plateau[0],lin,col)
    if possedePion(carte, numJoueur):
        prendrePion(carte, numJoueur)

def poserPionPlateau(plateau,lin,col,numJoueur):
    """
    met le pion du joueur sur la carte qui se trouve en (lig,col) du plateau
    paramètres: plateau:le plateau considéré
                lin: numéro de la ligne où se trouve le pion
                col: numéro de la colonne où se trouve le pion
                numJoueur: le numéro du joueur qui correspond au pion
    Cette fonction ne retourne rien mais elle modifie le plateau
    """
    
    carte = getVal(plateau[0],lin,col)
    #print(carte)
    poserPion(carte, numJoueur)


def accessible(plateau,ligD,colD,ligA,colA):
    """
    indique si il y a un chemin entre la case ligD,colD et la case ligA,colA du labyrinthe
    paramètres: plateau: le plateau considéré
                ligD: la ligne de la case de départ
                colD: la colonne de la case de départ
                ligA: la ligne de la case d'arrivée
                colA: la colonne de la case d'arrivée
    résultat: un boolean indiquant s'il existe un chemin entre la case de départ
              et la case d'arrivée
    """
    plateau = plateau[0]
    calque = Matrice(7,7,0)
    setVal(calque,ligD,colD,1)
    
    l_max = 7
    c_max = 7
    valeur = None
    
    marquer = True
    while marquer:
        marquer = False
        for ligne in range(l_max):
            colonne = 0
            for colonne in range(c_max):
                
                Nord = ligne-1
                Sud = ligne+1
                Est = colonne+1
                Ouest = colonne-1
                
                if getVal(calque,ligne,colonne)!=0:
                    
                    if Nord >= 0 and passageNord(plateau['Matrice'][ligne][colonne],plateau['Matrice'][Nord][colonne]) and getVal(calque,Nord,colonne)==0:
                        
                        marquer = True
                        
                        valeur = getVal(calque,ligne,colonne)
                        setVal(calque,Nord,colonne,valeur+1)
                        
                        #print("Nord")
                    
                    if Ouest >= 0 and passageOuest(plateau['Matrice'][ligne][colonne],plateau['Matrice'][ligne][Ouest]) and getVal(calque,ligne,Ouest)==0:
                        
                        marquer = True
                        
                        valeur = getVal(calque, ligne, colonne)
                        setVal(calque, ligne, Ouest, valeur+1)
                        
                        #print("Ouest")
                    
                    if Sud < l_max and passageSud(plateau['Matrice'][ligne][colonne],plateau['Matrice'][Sud][colonne]) and getVal(calque,Sud,colonne)==0:
                        
                        marquer = True
                        
                        valeur = getVal(calque, ligne, colonne)
                        setVal(calque, Sud, colonne, valeur+1)
                        
                        #print("Sud")
                    

                    if Est < c_max and passageEst(plateau['Matrice'][ligne][colonne],plateau['Matrice'][ligne][Est]) and getVal(calque,ligne,Est)==0:
                        
                        marquer = True
                        
                        valeur = getVal(calque, ligne, colonne)
                        setVal(calque, ligne, Est, valeur+1)
                        
                        #print("Est")
                    
    if getVal(calque,ligA,colA)!=0 and getVal(calque,ligD,colD)!=0:
        return True
    return False

#print(accessible(P0,0,0,2,2))

def accessibleDist(plateau,ligD,colD,ligA,colA):
    """
    indique si il y a un chemin entre la case ligD,colD et la case ligA,colA du plateau
    mais la valeur de retour est None s'il n'y a pas de chemin, 
    sinon c'est un chemin possible entre ces deux cases sous la forme d'une liste
    de coordonées (couple de (lig,col))
    paramètres: plateau: le plateau considéré
                ligD: la ligne de la case de départ
                colD: la colonne de la case de départ
                ligA: la ligne de la case d'arrivée
                colA: la colonne de la case d'arrivée
    résultat: une liste de coordonées indiquant un chemin possible entre la case
              de départ et la case d'arrivée
    """
    ####################################################################
    ### Cette fonction est beaucoup trop longue pour être maintenue  ###
    ### Efficacement, donc si vous avec une après midi a y consacrer ###
    ### Vous pouvez tenter de la factoriser en écrivant par exemple  ###
    ### Quelques fonctions auxiliaires, .o?                          ###
    ####################################################################
    plateau = plateau[0]
    calque = Matrice(7,7,0)
    setVal(calque,ligD,colD,1)
    
    l_max = 7
    c_max = 7
    valeur = None
    
    marquer = True
    while marquer:
        marquer = False
        for ligne in range(l_max):
            colonne = 0
            for colonne in range(c_max):
                
                Nord = ligne-1
                Sud = ligne+1
                Est = colonne+1
                Ouest = colonne-1
                
                if getVal(calque,ligne,colonne)!=0:
                    
                    if Nord >= 0 and passageNord(plateau['Matrice'][ligne][colonne],plateau['Matrice'][Nord][colonne]) and getVal(calque,Nord,colonne)==0:
                        
                        marquer = True
                        
                        valeur = getVal(calque,ligne,colonne)
                        setVal(calque,Nord,colonne,valeur+1)
                        
                        #print("Nord")
                    
                    if Ouest >= 0 and passageOuest(plateau['Matrice'][ligne][colonne],plateau['Matrice'][ligne][Ouest]) and getVal(calque,ligne,Ouest)==0:
                        
                        marquer = True
                        
                        valeur = getVal(calque, ligne, colonne)
                        setVal(calque, ligne, Ouest, valeur+1)
                        
                        #print("Ouest")
                    
                    if Sud < l_max and passageSud(plateau['Matrice'][ligne][colonne],plateau['Matrice'][Sud][colonne]) and getVal(calque,Sud,colonne)==0:
                        
                        marquer = True
                        
                        valeur = getVal(calque, ligne, colonne)
                        setVal(calque, Sud, colonne, valeur+1)
                        
                        #print("Sud")
                    

                    if Est < c_max and passageEst(plateau['Matrice'][ligne][colonne],plateau['Matrice'][ligne][Est]) and getVal(calque,ligne,Est)==0:
                        
                        marquer = True
                        
                        valeur = getVal(calque, ligne, colonne)
                        setVal(calque, ligne, Est, valeur+1)
                        
                        #print("Est")
    
    #print(getVal(calque,ligA,colA))
    #print(getVal(calque,ligD,colD))
    
    if getVal(calque,ligA,colA)==0 or getVal(calque,ligD,colD)==0:
        return None
    
    liste_coords = [(ligA,colA)]
    
    # On part de l'arrivée et on remonte jusqu'à la case 1, on ne peut pas le faire dans l'autre sens !
    
    while liste_coords[-1]!=(ligD,colD):
        ligne = liste_coords[-1][0]
        colonne = liste_coords[-1][1]
        valeur = getVal(calque, ligne, colonne)
        
        Nord = ligne -1
        Sud = ligne +1
        Ouest = colonne -1
        Est = colonne +1
        
        if Nord >= 0 and getVal(calque, Nord, colonne)==valeur-1 and passageNord(getVal(plateau, ligne, colonne), getVal(plateau, Nord, colonne)):
            liste_coords.append((Nord, colonne))
        
        else:
            if Sud < 7 and getVal(calque, Sud, colonne)==valeur-1 and passageSud(getVal(plateau, ligne, colonne), getVal(plateau, Sud, colonne)):
                liste_coords.append((Sud, colonne))
            
            else:
                if Ouest >= 0 and getVal(calque, ligne, Ouest)==valeur -1 and passageOuest(getVal(plateau, ligne, colonne), getVal(plateau, ligne, Ouest)):
                    liste_coords.append((ligne, Ouest))
                
                else:
                    if Est < 7 and getVal(calque, ligne, Est)==valeur -1 and passageEst(getVal(plateau, ligne, colonne), getVal(plateau, ligne, Est)):
                        liste_coords.append((ligne, Est))
                    
                    else:
                        
                        print("Erreur Dramatique qui ne devrait jamais arriver!")
                        print("Contacter d'urgence le 06 41 24 11 20 si c'est le Cas!")
                        print("Ceci est un message sérieux! Bien que conservant un caractère humouristique")
                        print("Mais vraiment si le programme entre dans ce else, on est mal jeunes gens!")
    #print((ligD,colD,ligA,colA))
    #print(liste_coords)
    liste_coords.reverse()
    return liste_coords

#print(accessibleDist(P0,2,1,3,3))

def getNbTresorsPlateau(plateau):
    """
    Fonction qui retourne le nombre de trésors restant sur le plateau
    """
    nbTresor = 0
    for elem in plateau[0]['Matrice']:
        if getTresor(elem) == 0:
            nbTresor += 1
    if getTresor(plateau[1]) == 0:
        nbTresor +=1
    return nbTresor

def setCarteAJouer(plateau, carte):
    """
    Attention, cette fonction détruit la carte à jouer précédente,
    Elle ne doit être appelée que dans la fonction JouerCarte de labyrinthe
    """
    plateau[1] = carte

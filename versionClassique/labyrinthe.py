# -*- coding: utf-8 -*-
"""
                           Projet Labyrinthe 
        Projet Python 2019-2020 de 1ere année et AS DUT Informatique Orléans
        
   Module labyrinthe
   ~~~~~~~~~~~~~~~~~
   
   Ce module gère sur le jeu du labyrinthe (observation et mise à jour du jeu).
"""

from listeJoueurs import *
from plateau import *

def Labyrinthe(nomsJoueurs=["joueur1","joueurs2"],nbTresors=24, nbTresorsMax=0):
    """
    permet de créer un labyrinthe avec nbJoueurs joueurs, nbTresors trésors
    chacun des joueurs aura au plus nbTresorMax à trouver
    si ce dernier paramètre est à 0, on distribuera le maximum de trésors possible 
    à chaque joueur en restant équitable
    un joueur courant est choisi et la phase est initialisée
    paramètres: nomsJoueurs est la liste des noms des joueurs participant à la partie (entre 1 et 4)
                nbTresors le nombre de trésors différents il en faut au moins 12 et au plus 49
                nbTresorMax le nombre de trésors maximum distribué à chaque joueur
    résultat: le labyrinthe crée, une liste contenant:
              à l'indice [0]: le plateau, une liste contenant la matrice et la carte à jouer (cf. plateau.py fonction Plateau)
              à l'indice [1]: la liste de jeu, une liste (cf. listeJoueur.py fonction ListeJoueur)
              à l'indice [2]: le numéro de phase, un entier correspondant au numéro de la phase de jeu
              à l'indice [3]: un tuple direction qui vaut N,E,O ou S et un entier qui 1,3, ou 5. Ce tuple correpond au coup précédent lors de la phase 1
    """
    #préparation de la liste de joueurs
    
    liste_joueurs=ListeJoueurs(nomsJoueurs)#initialisation d'une variable pour créer une liste de dico de joueurs(cf. fonction Listejoueurs dans listeJoueurs.py)
    
    distribuerTresors(liste_joueurs,nbTresors,nbTresorsMax)#distribuer les trésors aléatoirement à chaque joueurs(cf. fonction distribuerTresors dans listeJoueurs.py)
    
    #préparation du plateau
    nb_joueurs=len(nomsJoueurs)#nb_joueurs a pour valeur le nombre de joueurs dans la partie
    plateau=Plateau(nb_joueurs,nbTresors)#initialisation d'une variable pour créer le plateau(cf. Plateau dans plateau.py)
    
    #création du labyrinthe
    numero_de_phase=1
    
    rangee = None
    direction = None
    
    
    labyrinthe=[plateau,liste_joueurs,numero_de_phase,(direction,rangee)]
    initAleatoireJoueurCourant(getListeJoueurs(labyrinthe))
    
    return labyrinthe



def getPlateau(labyrinthe):
    """
    retourne la matrice représentant le plateau de jeu
    paramètre: labyrinthe le labyrinthe considéré
    résultat: la matrice représentant le plateau de ce labyrinthe avec la carte à jouer
    """
    return labyrinthe[0] #dans la mesure où plateau renvoi un tuple avec un dico

def getMatrice(labyrinthe):
    """
    retourne la matrice représentant le plateau de jeu
    paramètre: labyrinthe le labyrinthe considéré
    résultat: la matrice représentant le plateau de ce labyrinthe avec la carte à jouer
    """
    return labyrinthe[0][0]


def getNbParticipants(labyrinthe):
    """
    retourne le nombre de joueurs engagés dans la partie
    paramètre: labyrinthe le labyrinthe considéré
    résultat: le nombre de joueurs de la partie
    """
    return getNbJoueurs(labyrinthe[1])

def getNomJoueurCourant(labyrinthe):
    """
    retourne le nom du joueur courant
    paramètre: labyrinthe le labyrinthe considéré
    résultat: le nom du joueurs courant
    """
    return nomJoueurCourant(labyrinthe[1]) 

def getNumJoueurCourant(labyrinthe):
    """
    retourne le numero du joueur courant
    paramètre: labyrinthe le labyrinthe considéré
    résultat: le numero du joueurs courant
    """
    return numJoueurCourant(labyrinthe[1]) 

def getPhase(labyrinthe):
    """
    retourne la phase du jeu courante
    paramètre: labyrinthe le labyrinthe considéré
    résultat: le numéro de la phase de jeu courante
    """
    return labyrinthe[2] 

def changerPhase(labyrinthe):
    """
    change de phase de jeu en passant la suivante
    paramètre: labyrinthe le labyrinthe considéré
    la fonction ne retourne rien mais modifie le labyrinthe
    """
    phase = getPhase(labyrinthe)
    
    if phase == 1 or phase == None:
        labyrinthe[2] += 1
    else:
        if phase == 2:
            labyrinthe[2] -= 1
        else:
            print("erreur numéro phase")


def getNbTresors(labyrinthe):
    """
    retourne le nombre de trésors qu'il reste sur le labyrinthe
    paramètre: labyrinthe le labyrinthe considéré
    résultat: le nombre de trésors sur le plateau
    """
    return getNbTresorsPlateau(labyrinthe[0]) 

def getListeJoueurs(labyrinthe):
    """
    retourne la liste joueur structures qui gèrent les joueurs et leurs trésors a trouver
    paramètre: labyrinthe le labyrinthe considéré
    résultat: les joueurs sous la forme de la structure implémentée dans listeJoueurs.py    
    """
    return labyrinthe[1]


def enleverTresor(labyrinthe,lin,col,numTresor):
    """
    enleve le trésor numTresor du plateau du labyrinthe. 
    Si l'opération s'est bien passée le nombre total de trésors dans le labyrinthe
    est diminué de 1
    paramètres: labyrinthe: le labyrinthe considéré
                lig: la ligne où se trouve la carte
                col: la colonne où se trouve la carte
                numTresor: le numéro du trésor à prendre sur la carte
    la fonction ne retourne rien mais modifie le labyrinthe
    """
    prendreTresorPlateau(getPlateau(labyrinthe),lin,col,numTresor)

def prendreJoueurCourant(labyrinthe,lin,col):
    """
    enlève le joueur courant de la carte qui se trouve sur la case lin,col du plateau
    si le joueur ne s'y trouve pas la fonction ne fait rien
    paramètres: labyrinthe: le labyrinthe considéré
                lig: la ligne où se trouve la carte
                col: la colonne où se trouve la carte
    la fonction ne retourne rien mais modifie le labyrinthe    
    """
    prendrePionPlateau(getPlateau(labyrinthe),lin,col, getNumJoueurCourant(labyrinthe))

def poserJoueurCourant(labyrinthe,lin,col):
    """
    pose le joueur courant sur la case lin,col du plateau
    paramètres: labyrinthe: le labyrinthe considéré
                lig: la ligne où se trouve la carte
                col: la colonne où se trouve la carte
    la fonction ne retourne rien mais modifie le labyrinthe     
    """
    poserPionPlateau(getPlateau(labyrinthe),lin, col, getNumJoueurCourant(labyrinthe))

def getCarteAJouer(labyrinthe):
    """
    donne la carte à jouer
    paramètre: labyrinthe: le labyrinthe considéré
    résultat: la carte à jouer    
    """    
    return getPlateau(labyrinthe)[1]

def coupInterdit(labyrinthe,direction,rangee):
    """ 
    retourne True si le coup proposé correspond au coup interdit
    elle retourne False sinon
    paramètres: labyrinthe: le labyrinthe considéré
                direction: un caractère qui indique la direction choisie ('N','S','E','O')
                rangee: le numéro de la ligne ou de la colonne choisie
    résultat: un booléen indiquant si le coup est interdit ou non
    """
    
    (direc_prec, rangee_prec) = getCoupPrecedent(labyrinthe,direction,rangee)
    
    if (direc_prec,rangee_prec) == (None,None):
        return False
    else:
        if rangee_prec == rangee:
            if (direc_prec == 'N' and direction == 'S') or (direc_prec == 'S' and direction == 'N'):
                return True
            if (direc_prec == 'O' and direction == 'E') or (direc_prec == 'E' and direction == 'O'):
                return True
        else:
            return False

def jouerCarte(labyrinthe,direction,rangee):
    """
    fonction qui joue la carte amovible dans la direction et sur la rangée passées 
    en paramètres. Cette fonction
       - met à jour le plateau du labyrinthe
       - met à jour la carte à jouer
       - met à jour la nouvelle direction interdite
    paramètres: labyrinthe: le labyrinthe considéré
                direction: un caractère qui indique la direction choisie ('N','S','E','O')
                rangee: le numéro de la ligne ou de la colonne choisie
    Cette fonction ne retourne pas de résultat mais mais à jour le labyrinthe
    """
    if direction == 'N':
        carte = decalageColonneEnBas(getPlateau(labyrinthe)[0],rangee,getCarteAJouer(labyrinthe))
        if (getListePions(carte) != []):
            
            pions = getListePions(carte)
            for elem in pions:
                poserPionPlateau(getPlateau(labyrinthe), 0, rangee, elem)
            for elem in pions:
                setListePions(carte, [])
                
    if direction == 'S':
        carte = decalageColonneEnHaut(getPlateau(labyrinthe)[0],rangee,getCarteAJouer(labyrinthe))        
        if (getListePions(carte) != []):
            
            pions = getListePions(carte)
            for elem in pions:
                poserPionPlateau(getPlateau(labyrinthe), 6, rangee, elem)
            for elem in pions:
                setListePions(carte, [])


    
    if direction == 'E':
        carte = decalageLigneAGauche(getPlateau(labyrinthe)[0],rangee,getCarteAJouer(labyrinthe))
        if (getListePions(carte) != []):
            
            pions = getListePions(carte)
            for elem in pions:
                poserPionPlateau(getPlateau(labyrinthe), rangee, 6, elem)
            for elem in pions:
                setListePions(carte, [])

    if direction == 'O':
        carte = decalageLigneADroite(getPlateau(labyrinthe)[0],rangee,getCarteAJouer(labyrinthe)) 
        if (getListePions(carte) != []):
            
            pions = getListePions(carte)
            for elem in pions:
                poserPionPlateau(getPlateau(labyrinthe), rangee, 0, elem)
            for elem in pions:
                setListePions(carte, [])
    
    setCarteAJouer(getPlateau(labyrinthe),carte)
    setCoupPrecedent(labyrinthe, direction, rangee)
    
        

def setCoupPrecedent(labyrinthe, direction, rangee):
    """
    Met dans labyrinthe la valeur du coup precédent
    """
    
    labyrinthe[3] = (direction,rangee)

def getCoupPrecedent(labyrinthe, direction, rangee):
    """
    Récupère les info du coup précédent
    """
    
    return labyrinthe[3]

def tournerCarte(labyrinthe,sens='H'):
    """
    tourne la carte à jouer dans le sens indiqué en paramètre (H horaire A antihoraire)
    paramètres: labyrinthe: le labyrinthe considéré
                sens: un caractère indiquant le sens dans lequel tourner la carte
     Cette fonction ne retourne pas de résultat mais mais à jour le labyrinthe    
    """
    if sens == 'H':
        tournerHoraire(getCarteAJouer(labyrinthe))
    if sens == 'A':
        tournerAntiHoraire(getCarteAJouer(labyrinthe))

def getTresorCourant(labyrinthe):
    """
    retourne le numéro du trésor que doit cherche le joueur courant
    paramètre: labyrinthe: le labyrinthe considéré 
    resultat: le numéro du trésor recherché par le joueur courant
    """
    return prochainTresorJoueur(getListeJoueurs(labyrinthe),getNumJoueurCourant(labyrinthe))

def getCoordonneesTresorCourant(labyrinthe):
    """
    donne les coordonnées du trésor que le joueur courant doit trouver
    paramètre: labyrinthe: le labyrinthe considéré 
    resultat: les coordonnées du trésor à chercher ou None si celui-ci 
              n'est pas sur le plateau
    """
    return getCoordonneesTresor(getPlateau(labyrinthe), getTresorCourant(labyrinthe))


def getCoordonneesJoueurCourant(labyrinthe):
    """
    donne les coordonnées du joueur courant sur le plateau
    paramètre: labyrinthe: le labyrinthe considéré 
    resultat: les coordonnées du joueur courant ou None si celui-ci 
              n'est pas sur le plateau
    """
    return getCoordonneesJoueur(getPlateau(labyrinthe), getNumJoueurCourant(labyrinthe))


def executerActionPhase1(labyrinthe,action,rangee):
    """
    exécute une action de jeu de la phase 1
    paramètres: labyrinthe: le labyrinthe considéré
                action: un caractère indiquant l'action à effecter
                        si action vaut 'T' => faire tourner la carte à jouer
                        si action est une des lettres N E S O et rangee est un des chiffre 1,3,5 
                        => insèrer la carte à jouer à la direction action sur la rangée rangee
                           et faire le nécessaire pour passer en phase 2
    résultat: un entier qui vaut
              0 si l'action demandée était valide et demandait de tourner la carte
              1 si l'action demandée était valide et demandait d'insérer la carte
              2 si l'action est interdite car l'opposée de l'action précédente
              3 si action et rangee sont des entiers positifs
              4 dans tous les autres cas
    """
    lettres_valides = {'N','O','E','S'}
    rangees_valides = {1,3,5}
    
    try :
        action = action.upper()
        
    except AttributeError:
        return 4
    
    if (action != -1 or (rangee == None or rangee in rangees_valides)):
        # Les inputs ont la bonne taille pour être valide
        
        if action == 'T':
            tournerCarte(labyrinthe)
            return 0
        if action in lettres_valides and rangee in rangees_valides:
            
            if coupInterdit(labyrinthe, action, rangee):
                return 2
            else:
                jouerCarte(labyrinthe, action, rangee)
                changerPhase(labyrinthe)
                return 1
        else:
            if action.isnumeric() and rangee.isnumeric():
                return 3
            else:
                return 4
    else:
        # Un des inputs (ou les deux) font plus qu'un caractère
        print("plop")
        return 4
    

def accessibleDistJoueurCourant(labyrinthe, ligA,colA):
    """
    verifie si le joueur courant peut accéder la case ligA,colA
    si c'est le cas la fonction retourne une liste représentant un chemin possible
    sinon ce n'est pas le cas, la fonction retourne None
    paramètres: labyrinthe le labyrinthe considéré
                ligA la ligne de la case d'arrivée
                colA la colonne de la case d'arrivée
    résultat: une liste de couples d'entier représentant un chemin que le joueur
              courant atteigne la case d'arrivée s'il existe None si pas de chemin
    """
    (ligD,colD) = getCoordonneesJoueurCourant(labyrinthe)
    return accessibleDist(getPlateau(labyrinthe), ligD, colD, ligA, colA)

def finirTour(labyrinthe):
    """
    vérifie si le joueur courant vient de trouver un trésor (si oui fait le nécessaire)
    vérifie si la partie est terminée, si ce n'est pas le cas passe au joueur suivant
    paramètre: labyrinthe le labyrinthe considéré
    résultat: un entier qui vaut
              0 si le joueur courant n'a pas trouvé de trésor
              1 si le joueur courant a trouvé un trésor mais la partie n'est pas terminée
              2 si le joueur courant a trouvé son dernier trésor (la partie est donc terminée)
    """
    (i,j) = getCoordonneesJoueurCourant(labyrinthe)
    if (i,j) == getCoordonneesTresorCourant(labyrinthe):
        
        enleverTresor(labyrinthe, i, j, getTresorCourant(labyrinthe))
        
        
        
        joueurCourantTrouveTresor(getListeJoueurs(labyrinthe))
        
        if joueurCourantAFini(getListeJoueurs(labyrinthe)):
            return 2
        else:
            changerPhase(labyrinthe)
            changerJoueurCourant(labyrinthe[1])
            return 1
        
    else:
        changerJoueurCourant(labyrinthe[1])
        changerPhase(labyrinthe)
        return 0

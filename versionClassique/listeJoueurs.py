# -*- coding: utf-8 -*-
"""
                           Projet Labyrinthe 
        Projet Python 2019-2020 de 1ere année et AS DUT Informatique Orléans
        
   Module listeJoueurs
   ~~~~~~~~~~~~~~~~~~~
   
   Ce module gère la liste des joueurs. 
"""

import random
from joueur import *

def ListeJoueurs(nomsJoueurs):
    """
    créer une liste de joueurs dont les noms sont dans la liste de noms passés en paramètre
    Attention il s'agit d'une liste de joueurs qui gère la notion de joueur courant
    paramètre: nomsJoueurs une liste de chaines de caractères
    résultat: la liste des joueurs avec un joueur courant mis à 0
    """
    liste_de_jeu=[]
    liste_joueurs=[]
    for nom in nomsJoueurs:
        dico_joueur=Joueur(nom)
        liste_joueurs.append(dico_joueur)
    liste_de_jeu.append(0)
    liste_de_jeu.append(liste_joueurs)
    
    return liste_de_jeu
    

def ajouterJoueur(joueurs, joueur):
    """
    ajoute un nouveau joueur à la fin de la liste
    paramètres: joueurs une liste de joueurs
                joueur le joueur à ajouter
    cette fonction ne retourne rien mais modifie la liste des joueurs
    """

    dico_nouveau_joueur=Joueur(joueur)
    joueurs[1].append(dico_nouveau_joueur)
 

    

def initAleatoireJoueurCourant(joueurs):
    """
    tire au sort le joueur courant
    paramètre: joueurs un liste de joueurs
    cette fonction ne retourne rien mais modifie la liste des joueurs
    """
    numero_joueur_courant=random.randint(0,getNbJoueurs(joueurs)-1)
    joueurs[0]=numero_joueur_courant
    
def distribuerTresors(joueurs,nbTresors=24, nbTresorMax=0):
    """
    distribue de manière aléatoire des trésors entre les joueurs.
    paramètres: joueurs la liste des joueurs
                nbTresors le nombre total de trésors à distribuer (on rappelle 
                        que les trésors sont des entiers de 1 à nbTresors)
                nbTresorsMax un entier fixant le nombre maximum de trésor 
                             qu'un joueur aura après la distribution
                             si ce paramètre vaut 0 on distribue le maximum
                             de trésor possible  
    cette fonction ne retourne rien mais modifie la liste des joueurs
    """
   
    nb_joueurs=len(joueurs[1])
    
    tresors_par_joueur=[]
    numero_du_joueur=1
    nb_tresor_max=0
    if nbTresorMax==0 or nbTresorMax*nb_joueurs>nbTresors:
        nb_tresor_max=nbTresors//nb_joueurs
    else:
        nb_tresor_max=nbTresorMax
    
    tresor_a_distribuer=random.sample(range(1,nb_tresor_max*nb_joueurs+1),nb_tresor_max*nb_joueurs)
    
    num_joueur = 0
    for i in tresor_a_distribuer:
        ajouterTresor(joueurs[1][num_joueur],i)
        num_joueur = (num_joueur+1)%getNbJoueurs(joueurs)


def changerJoueurCourant(joueurs):
    """
    passe au joueur suivant (change le joueur courant donc)
    paramètres: joueurs la liste des joueurs
    cette fonction ne retourne rien mais modifie la liste des joueurs
    """
    joueurs[0]=(joueurs[0]+1)%len(joueurs[1])
        

def getNbJoueurs(joueurs):
    """
    retourne le nombre de joueurs participant à la partie
    paramètre: joueurs la liste des joueurs
    résultat: le nombre de joueurs de la partie
    """
  
    return len(joueurs[1])

def getJoueurCourant(joueurs):
    """
    retourne le joueur courant
    paramètre: joueurs la liste des joueurs
    résultat: le joueur courant
    """
    
    return joueurs[1][joueurs[0]]

def joueurCourantTrouveTresor(joueurs):
    """
    Met à jour le joueur courant lorsqu'il a trouvé un trésor
    c-à-d enlève le trésor de sa liste de trésors à trouver
    paramètre: joueurs la liste des joueurs
    cette fonction ne retourne rien mais modifie la liste des joueurs
    """
    getJoueurCourant(joueurs)["ListeDeTresor"].remove(getJoueurCourant(joueurs)["ListeDeTresor"][0])

def nbTresorsRestantsJoueur(joueurs,numJoueur):
    """
    retourne le nombre de trésors restant pour le joueur dont le numéro 
    est donné en paramètre
    paramètres: joueurs la liste des joueurs
                numJoueur le numéro du joueur
    résultat: le nombre de trésors que joueur numJoueur doit encore trouver
    """
  
    return len(joueurs[1][numJoueur-1]["ListeDeTresor"])

def numJoueurCourant(joueurs):
    """
    retourne le numéro du joueur courant
    paramètre: joueurs la liste des joueurs
    résultat: le numéro du joueur courant
    """
    
    return joueurs[0]+1
    

def nomJoueurCourant(joueurs):
    """
    retourne le nom du joueur courant
    paramètre: joueurs la liste des joueurs
    résultat: le nom du joueur courant
    """

    return getNom(joueurs[1][joueurs[0]])

def nomJoueur(joueurs,numJoueur):
    """
    retourne le nom du joueur dont le numero est donné en paramètre
    paramètres: joueurs la liste des joueurs
                numJoueur le numéro du joueur    
    résultat: le nom du joueur numJoueur
    """
    return getNom(joueurs[1][numJoueur-1])

def prochainTresorJoueur(joueurs,numJoueur):
    """
    retourne le trésor courant du joueur dont le numero est donné en paramètre
    paramètres: joueurs la liste des joueurs
                numJoueur le numéro du joueur    
    résultat: le prochain trésor du joueur numJoueur (un entier)
    """
    if joueurs[1][numJoueur-1]['ListeDeTresor'] != []:
        return joueurs[1][numJoueur-1]['ListeDeTresor'][0]
    else:
        return None

def tresorCourant(joueurs):
    """
    retourne le trésor courant du joueur courant
    paramètre: joueurs la liste des joueurs 
    résultat: le prochain trésor du joueur courant (un entier)
    """
    if getJoueurCourant(joueurs)['ListeDeTresor'] != []:
        return getJoueurCourant(joueurs)['ListeDeTresor'][0]
    else:
        return None
    

def joueurCourantAFini(joueurs):
    """
    indique si le joueur courant a gagné
    paramètre: joueurs la liste des joueurs 
    résultat: un booleen indiquant si le joueur courant a fini
    """
    
    a_gagne=False
    if len(getJoueurCourant(joueurs)['ListeDeTresor'])==0:
        a_gagne=True
    return a_gagne

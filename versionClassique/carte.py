# -*- coding: utf-8 -*-
"""
                           Projet Labyrinthe 
        Projet Python 2019-2020 de 1ere année et AS DUT Informatique Orléans
        
   Module carte
   ~~~~~~~~~~~~
   
   Ce module gère les cartes du labyrinthe. 
"""
import random


"""
la liste des caractères semi-graphiques correspondant aux différentes cartes
l'indice du caractère dans la liste correspond au codage des murs sur la carte
le caractère 'Ø' indique que l'indice ne correspond pas à une carte
"""
listeCartes=['╬','╦','╣','╗','╩','═','╝','Ø','╠','╔','║','Ø','╚','Ø','Ø','Ø']
# Numerotation des case de la liste de 0 a 15, on peut donc le coder avec cadre booleen comme suit :
# index 0 == 0000, index 1 == 0001, index 2 == 0010, index 3 == 0011 etc jusque index 15 == 1111

def Carte( nord, est, sud, ouest, tresor=0, pions=[]):
    """
    permet de créer une carte:
    paramètres:
    nord, est, sud et ouest sont des booléens indiquant s'il y a un mur ou non dans chaque direction
    tresor est le numéro du trésor qui se trouve sur la carte (0 s'il n'y a pas de trésor)
    pions est la liste des pions qui sont posés sur la carte (un pion est un entier entre 1 et 4)
    """
    dict_cart = {}
    dict_cart['Nord']=nord
    dict_cart['Sud']=sud
    dict_cart['Est']=est
    dict_cart['Ouest']=ouest
    dict_cart['Tresor']=tresor
    dict_cart['Pions']=pions
    return dict_cart

assert Carte(False,True,False,True,1,[1,3])=={'Nord':False,'Est':True,'Sud':False,'Ouest':True,'Tresor':1,'Pions':[1,3]}

def estValide(c):
    """
    retourne un booléen indiquant si la carte est valide ou non c'est à dire qu'elle a zéro un ou deux murs
    paramètre: c une carte
    """
    res = 0
    if c['Nord']:
        res += 1
    if c['Sud']:
        res += 1
    if c['Est']:
        res += 1
    if c['Ouest']:
        res += 1
    if res < 3:
        return True
    return False

carte_test = Carte(False,False,False,True,4,[1,2])
carte_test2 = Carte(False,False,False,False,0,[])
assert estValide(carte_test)
assert estValide(carte_test2)

def murNord(c):
    """
    retourne un booléen indiquant si la carte possède un mur au nord
    paramètre: c une carte
    """
    if c['Nord']:
        return True
    return False

carte_test['Nord']=True
assert murNord(carte_test)
assert not murNord(carte_test2)

def murSud(c):
    """
    retourne un booléen indiquant si la carte possède un mur au sud
    paramètre: c une carte
    """
    if c['Sud']:
        return True
    return False

def murEst(c):
    """
    retourne un booléen indiquant si la carte possède un mur à l'est
    paramètre: c une carte
    """
    if c['Est']:
        return True
    return False

def murOuest(c):
    """
    retourne un booléen indiquant si la carte possède un mur à l'ouest
    paramètre: c une carte
    """
    if c['Ouest']:
        return True
    return False

def getListePions(c):
    """
    retourne la liste des pions se trouvant sur la carte
    paramètre: c une carte
    """
    return c['Pions']

assert getListePions(carte_test)==[1,2]

def setListePions(c,listePions):
    """
    place la liste des pions passées en paramètre sur la carte
    paramètres: c: est une carte
                listePions: la liste des pions à poser
    Cette fonction ne retourne rien mais modifie la carte
    """
    c['Pions']=listePions

setListePions(carte_test2,[2,3])
assert getListePions(carte_test2)==[2,3]

def getNbPions(c):
    """
    retourne le nombre de pions se trouvant sur la carte
    paramètre: c une carte
    """
    return len(c['Pions'])

assert getNbPions(carte_test)==2

def possedePion(c,pion):
    """
    retourne un booléen indiquant si la carte possède le pion passé en paramètre
    paramètres: c une carte
                pion un entier compris entre 1 et 4
    """
    if pion in getListePions(c):
        return True
    return False

assert not possedePion(carte_test, 3)
assert possedePion(carte_test, 2)

def getTresor(c):
    """
    retourne la valeur du trésor qui se trouve sur la carte (0 si pas de trésor)
    paramètre: c une carte
    """
    return c['Tresor']

assert getTresor(carte_test)==4

def prendreTresor(c):
    """
    enlève le trésor qui se trouve sur la carte et retourne la valeur de ce trésor
    paramètre: c une carte
    résultat l'entier représentant le trésor qui était sur la carte
    """
    val = c['Tresor']
    c['Tresor'] = 0
    return val

assert prendreTresor(carte_test)==4
assert getTresor(carte_test)==0
    
def mettreTresor(c,tresor):
    """
    met le trésor passé en paramètre sur la carte et retourne la valeur de l'ancien trésor
    paramètres: c une carte
                tresor un entier positif
    résultat l'entier représentant le trésor qui était sur la carte
    """
    val = c['Tresor']
    c['Tresor']=tresor
    return val

assert mettreTresor(carte_test,7)==0
assert getTresor(carte_test)==7

def prendrePion(c, pion):
    """
    enlève le pion passé en paramètre de la carte. Si le pion n'y était pas ne fait rien
    paramètres: c une carte
                pion un entier compris entre 1 et 4
    Cette fonction modifie la carte mais ne retourne rien
    """
    if pion in c['Pions']:
        c['Pions'].remove(pion)

prendrePion(carte_test, 1)
assert getListePions(carte_test)==[2]

def poserPion(c, pion):
    """
    pose le pion passé en paramètre sur la carte. Si le pion y était déjà ne fait rien
    paramètres: c une carte
                pion un entier compris entre 1 et 4
    Cette fonction modifie la carte mais ne retourne rien
    """
    if pion not in c['Pions']:
        
        c['Pions'].append(pion)

poserPion(carte_test, 1)
assert sorted(getListePions(carte_test))==[1,2]

def tournerHoraire(c):
    """
    fait tourner la carte dans le sens horaire
    paramètres: c une carte
    Cette fonction modifie la carte mais ne retourne rien    
    """
    tmp = c['Nord'] 
    c['Nord'] = c['Ouest']
    c['Ouest'] = c['Sud']
    c['Sud'] = c['Est']
    c['Est'] = tmp

carte_test3 = Carte(True,False,True,False,0,[])
tournerHoraire(carte_test3)
assert not murNord(carte_test3)
assert not murSud(carte_test3)
assert murEst(carte_test3)
assert murOuest(carte_test3)

def tournerAntiHoraire(c):
    """
    fait tourner la carte dans le sens anti-horaire
    paramètres: c une carte
    Cette fonction modifie la carte mais ne retourne rien    
    """
    tournerHoraire(c)
    tournerHoraire(c)
    tournerHoraire(c)

tournerAntiHoraire(carte_test3)
assert murNord(carte_test3)
assert murSud(carte_test3)
assert not murEst(carte_test3)
assert not murOuest(carte_test3)

def tourneAleatoire(c):
    """
    faire tourner la carte d'un nombre de tours aléatoire
    paramètres: c une carte
    Cette fonction modifie la carte mais ne retourne rien    
    """
    nb_tour = random.randint(0,3)
    i = 0
    while i <= nb_tour:
        tournerHoraire(c)
        i += 1

def coderMurs(c):
    """
    code les murs sous la forme d'un entier dont le codage binaire 
    est de la forme bNbEbSbO où bN, bE, bS et bO valent 
       soit 0 s'il n'y a pas de mur dans dans la direction correspondante
       soit 1 s'il y a un mur dans la direction correspondante
    bN est le chiffre des unité, BE des dizaine, etc...
    le code obtenu permet d'obtenir l'indice du caractère semi-graphique
    correspondant à la carte dans la liste listeCartes au début de ce fichier
    paramètre c une carte
    retourne un entier indice du caractère semi-graphique de la carte
    """
    res = 0
    if murNord(c):
        res += 1
    if murEst(c):
        res += 2
    if murSud(c):
        res += 4
    if murOuest(c):
        res += 8
    
    return res

#listeCartes=['╬','╦','╣','╗','╩','═','╝','Ø','╠','╔','║','Ø','╚','Ø','Ø','Ø']
#              0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15

def decoderMurs(c,code):
    """
    positionne les murs d'une carte en fonction du code décrit précédemment
    paramètres c une carte
               code un entier codant les murs d'une carte
    Cette fonction modifie la carte mais ne retourne rien
    """
    
    if code-8 >= 0:
        c['Ouest']=True
        code = code-8
    else:
        c['Ouest']=False

    if code-4 >= 0:
        c['Sud']=True
        code = code-4
    else:
        c['Sud']=False

    if code-2 >= 0:
        c['Est']=True
        code = code-2
    else:
        c['Est']=False

    if code-1 >= 0:
        c['Nord']=True
        code = code-1
    else:
        c['Nord']=False


def toChar(c):
    """
    fournit le caractère semi graphique correspondant à la carte (voir la variable listeCartes au début de ce script)
    paramètres c une carte
    """
    indice = coderMurs(c)
    return listeCartes[indice]

carte_test4 = Carte(False,False,False,False,0,[])
assert toChar(carte_test4)=="╬"

####################################
## Pour mémoire, fonction à modifier ici!!! j'ai observé un déplacement impossible lors d'une partie
## ╗  ou le pion est passée de la carte nord vers le sud
## ╦    Sachant que l'erreur peut aussi venir de accessibledist #Joie

def passageNord(carte1,carte2):
    """
    suppose que la carte2 est placée au nord de la carte1 et indique
    s'il y a un passage entre ces deux cartes en passant par le nord
    paramètres carte1 et carte2 deux cartes
    résultat un booléen
    """
    if carte1['Nord']==False and carte2['Sud']==False:
        return True
    return False

def passageSud(carte1,carte2):
    """
    suppose que la carte2 est placée au sud de la carte1 et indique
    s'il y a un passage entre ces deux cartes en passant par le sud
    paramètres carte1 et carte2 deux cartes
    résultat un booléen
    """
    if carte1['Sud']==False and carte2['Nord']==False:
        return True
    return False

def passageOuest(carte1,carte2):
    """
    suppose que la carte2 est placée à l'ouest de la carte1 et indique
    s'il y a un passage entre ces deux cartes en passant par l'ouest
    paramètres carte1 et carte2 deux cartes
    résultat un booléen
    """
    if carte1['Ouest']==False and carte2['Est']==False:
        return True
    return False

def passageEst(carte1,carte2):
    """
    suppose que la carte2 est placée à l'est de la carte1 et indique
    s'il y a un passage entre ces deux cartes en passant par l'est
    paramètres carte1 et carte2 deux cartes
    résultat un booléen    
    """
    if carte1['Est']==False and carte2['Ouest']==False:
        return True
    return False

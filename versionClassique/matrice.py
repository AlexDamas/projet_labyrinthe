# -*- coding: utf-8 -*-
"""
                           Projet Labyrinthe 
        Projet Python 2019-2020 de 1ere année et AS DUT Informatique Orléans
        
   Module matrice
   ~~~~~~~~~~~~~~~
   
   Ce module gère une matrice. 
"""

#-----------------------------------------
# contructeur et accesseurs
#-----------------------------------------

def Matrice(nbLignes,nbColonnes,valeurParDefaut=0):
    """
    crée une matrice de nbLignes lignes sur nbColonnes colonnes en mettant 
    valeurParDefaut dans chacune des cases
    paramètres: 
      nbLignes un entier strictement positif qui indique le nombre de lignes
      nbColonnes un entier strictement positif qui indique le nombre de colonnes
      valeurParDefaut la valeur par défaut
    résultat la matrice ayant les bonnes propriétés
    """
    mat = {'Lignes':nbLignes,'Colonnes':nbColonnes}
    valeurs = []
    for i in range(nbLignes):
        valeurs.append([valeurParDefaut]*nbColonnes)
    mat['Matrice']=valeurs
    return mat

assert Matrice(1,1,0)=={'Lignes':1,'Colonnes':1,'Matrice':[[0]]}
assert Matrice(2,2,2)=={'Lignes':2,'Colonnes':2,'Matrice':[[2,2],[2,2]]}

def getNbLignes(matrice):
    """
    retourne le nombre de lignes de la matrice
    paramètre: matrice la matrice considérée
    """
    return matrice['Lignes']
    
assert getNbLignes(Matrice(15,1,0))==15

def getNbColonnes(matrice):
    """
    retourne le nombre de colonnes de la matrice
    paramètre: matrice la matrice considérée
    """
    return matrice['Colonnes']

assert getNbColonnes(Matrice(1,15,0))==15

def getVal(matrice,ligne,colonne):
    """
    retourne la valeur qui se trouve en (ligne,colonne) dans la matrice
    paramètres: matrice la matrice considérée
                ligne le numéro de la ligne (en commençant par 0)
                colonne le numéro de la colonne (en commençant par 0)
    """
    valeurs = matrice['Matrice']
    ligne = int(ligne)
    colonne = int(colonne)
    res = valeurs[ligne][colonne]
    return res

assert getVal(Matrice(3,3,99),2,2)==99

def setVal(matrice,ligne,colonne,valeur):
    """
    met la valeur dans la case se trouve en (ligne,colonne) de la matrice
    paramètres: matrice la matrice considérée
                ligne le numéro de la ligne (en commençant par 0)
                colonne le numéro de la colonne (en commençant par 0)
                valeur la valeur à stocker dans la matrice
    cette fonction ne retourne rien mais modifie la matrice
    """
    valeurs = matrice['Matrice']
    valeurs[ligne][colonne] = valeur
    #return matrice

#assert setVal(Matrice(3,3,10),2,1,15)=={'Lignes':3,'Colonnes':3,'Matrice':[[10,10,10],[10,10,10],[10,15,10]]}


#------------------------------------------        
# decalages
#------------------------------------------
def decalageLigneAGauche(matrice, numLig, nouvelleValeur=0):
    """
    permet de décaler une ligne vers la gauche en insérant une nouvelle
    valeur pour remplacer la premiere case à droite de cette ligne
    le fonction retourne la valeur qui a été éjectée
    paramèteres: matrice la matrice considérée
                 numLig le numéro de la ligne à décaler
                 nouvelleValeur la valeur à placer
    résultat la valeur qui a été ejectée lors du décalage
    """
    longueur_liste = getNbColonnes(matrice)
    ligne_valeur = []
    for valeur in range(longueur_liste):
        ligne_valeur.append(getVal(matrice, numLig, valeur))

    res = ligne_valeur[0]
    valeur = 0
    for i in range(1,longueur_liste):
        ligne_valeur[i-1] = ligne_valeur[i]
        
    ligne_valeur[-1]=nouvelleValeur
    
    i = 0
    for i in range(0, longueur_liste):
        setVal(matrice, numLig, i, ligne_valeur[i])
    
    
    
    print("Valeur ligne : ",ligne_valeur)
    print("Valeur Res : ",res)
    return res

#test = Matrice(2,9,3)
#decalageLigneAGauche(test,1,8)
#decalageLigneAGauche(test,1,22)
#print(test)

def decalageLigneADroite(matrice, numLig, nouvelleValeur=0):
    """
    decale la ligne numLig d'une case vers la droite en insérant une nouvelle
    valeur pour remplacer la premiere case à gauche de cette ligne
    paramèteres: matrice la matrice considérée
                 numLig le numéro de la ligne à décaler
                 nouvelleValeur la valeur à placer
    résultat: la valeur de la case "ejectée" par le décalage
    """
    longueur_liste = getNbColonnes(matrice)
    ligne_valeur = []

    for valeur in range(longueur_liste):
        ligne_valeur.append(getVal(matrice, numLig, valeur))
        #setVal(matrice, numLig, valeur, ligne_valeur[-1])
    res = ligne_valeur[-1]
    
    # test avec insert
    ligne_valeur.insert(0,nouvelleValeur)
    ligne_valeur.pop()

    #valeur = 0
    #for i in range(1,longueur_liste-1):
    #    ligne_valeur[longueur_liste-i] = ligne_valeur[longueur_liste-i-1]
    
    #ligne_valeur[0]=nouvelleValeur
    
    for i in range(0, longueur_liste):
        setVal(matrice, numLig, i, ligne_valeur[i])
    
    #print(ligne_valeur)
    #print(res)
    return res

#decalageLigneADroite(test,8,6)


def decalageColonneEnHaut(matrice, numCol, nouvelleValeur=0):
    """
    decale la colonne numCol d'une case vers le haut en insérant une nouvelle
    valeur pour remplacer la premiere case en bas de cette ligne
    paramèteres: matrice la matrice considérée
                 numCol le numéro de la colonne à décaler
                 nouvelleValeur la valeur à placer
    résultat: la valeur de la case "ejectée" par le décalage
    """
    longueur_liste = getNbLignes(matrice)
    ligne_valeur = []
    for valeur in range(longueur_liste):
        ligne_valeur.append(getVal(matrice, valeur, numCol))

    res = ligne_valeur[0]
    
    for i in range(1,longueur_liste):
        ligne_valeur[i-1] = ligne_valeur[i]
        
    ligne_valeur[-1]=nouvelleValeur

    for i in range(0, longueur_liste):
        setVal(matrice, i, numCol, ligne_valeur[i])
    
    #print(ligne_valeur)
    #print(res)
    return res

#testCol = Matrice(4,5,2)
#decalageColonneEnHaut(testCol, 0,5)
#print(testCol)

def decalageColonneEnBas(matrice, numCol, nouvelleValeur=0):
    """
    decale la colonne numCol d'une case vers le bas en insérant une nouvelle
    valeur pour remplacer la premiere case en haut de cette ligne
    paramèteres: matrice la matrice considérée
                 numCol le numéro de la colonne à décaler
                 nouvelleValeur la valeur à placer
    résultat: la valeur de la case "ejectée" par le décalage
    """
    longueur_liste = getNbLignes(matrice)
    ligne_valeur = []

    for valeur in range(longueur_liste):
        ligne_valeur.append(getVal(matrice, valeur, numCol))
        #setVal(matrice, valeur, numCol, ligne_valeur[-1])
    res = ligne_valeur[-1]   
    
    # test avec insert
    ligne_valeur.insert(0,nouvelleValeur)
    ligne_valeur.pop()
    
    #for i in range(1,longueur_liste-1):
    #    ligne_valeur[longueur_liste-i] = ligne_valeur[longueur_liste-i-1]
    #
    #ligne_valeur[0]=nouvelleValeur
    
    
    
    for i in range(0, longueur_liste):
        setVal(matrice, i, numCol, ligne_valeur[i])
    
    #print(ligne_valeur)
    #print(res)
    return res

testCol = Matrice(3,3,0)
decalageColonneEnBas(testCol,1,34)
decalageColonneEnBas(testCol,1,32)
print(testCol)
